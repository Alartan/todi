import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Extras 1.4

Item {
    id: calendarForDate

    property alias closingButton: closingButton
    property alias acceptButton: acceptButton
    property alias calendar123: calendar123
    property alias calendarForDate: calendarForDate
    property alias inputHours: inputHours
    property alias minutesFielddown: minutesFielddown
    property alias minutesField: minutesField
    property alias minutesFieldup: minutesFieldup
    property alias hoursField: hoursField
    property alias hoursFielddown: hoursFielddown
    property alias hoursFieldup: hoursFieldup
    width: calendar123.width+7
    height: calendar123.height+7+buttonsBar.height
    clip: true

    Rectangle {
        id: calendarFrame
        width: calendar123.width+7
        height: calendar123.height+7
        color: "#ffffff"
        radius: 7
        anchors.top: parent.top
        anchors.left: parent.left
        border.width: 2
        border.color: "#400551"

        Calendar {
            id: calendar123
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            weekNumbersVisible: true
            z: 10
            selectedDate: chosen_Day
        }
    }



    Rectangle {
        id: buttonsBar
        width: calendarFrame.width
        height: calendarFrame.width/1.6+50
        color: "#f2eded"
        radius: 7
        z: -1
        anchors.top: calendarFrame.bottom
        anchors.left: calendarFrame.left
        border.color: "#400551"
        border.width: 2

        Row {
            id: inputHours
            height: calendarFrame.width/1.6
            anchors.top: buttonsBar.top
            spacing: 0
            anchors.horizontalCenter: parent.horizontalCenter



            Column {
                height: parent.height
                width: height/2

                Rectangle {
                    height: width/2
                    radius: 7
                    anchors.right: parent.right
                    anchors.left: parent.left
                    border.color: "#400551"
                    border.width: 2

                    MouseArea {
                        id: hoursFieldup
                        anchors.fill: parent
                    }

                    Image {
                        fillMode: Image.PreserveAspectFit
                        anchors.rightMargin: 10
                        anchors.leftMargin: 10
                        anchors.bottomMargin: 10
                        anchors.topMargin: 10
                        anchors.fill: parent
                        source: "Images/plus.png"
                    }
                }

                Rectangle {
                    id: rectangle1
                    height: width
                    radius: 7
                    border.color: "#400551"
                    anchors.right: parent.right
                    anchors.left: parent.left

                    Text {
                        id: hoursField
                        width: parent.width-6
                        text: "12"
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 50
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }

                Rectangle {
                    height: width/2
                    radius: 7
                    anchors.right: parent.right
                    anchors.left: parent.left
                    border.width: 2
                    MouseArea {
                        id: hoursFielddown
                        anchors.fill: parent
                    }

                    Image {
                        fillMode: Image.PreserveAspectFit
                        anchors.bottomMargin: 10
                        anchors.rightMargin: 10
                        source: "Images/minus.png"
                        anchors.leftMargin: 10
                        anchors.fill: parent
                        anchors.topMargin: 10
                    }
                    border.color: "#400551"
                }
            }

            Column {
                height: parent.height
                width: height/2

                Rectangle {
                    height: width/2
                    radius: 7
                    anchors.right: parent.right
                    anchors.left: parent.left
                    border.color: "#400551"
                    border.width: 2

                    MouseArea {
                        id: minutesFieldup
                        anchors.fill: parent
                    }

                    Image {
                        fillMode: Image.PreserveAspectFit
                        anchors.rightMargin: 10
                        anchors.leftMargin: 10
                        anchors.bottomMargin: 10
                        anchors.topMargin: 10
                        anchors.fill: parent
                        source: "Images/plus.png"
                    }
                }

                Rectangle {
                    height: width
                    radius: 7
                    border.color: "#400551"
                    anchors.right: parent.right
                    anchors.left: parent.left

                    Text {
                        id: minutesField
                        width: parent.width-6
                        text: "47"
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        anchors.rightMargin: 3
                        anchors.leftMargin: 3
                        anchors.fill: parent
                        font.pointSize: 50
                    }
                }

                Rectangle {
                    height: width/2
                    radius: 7
                    anchors.right: parent.right
                    anchors.left: parent.left
                    border.width: 2
                    MouseArea {
                        id: minutesFielddown
                        anchors.fill: parent
                    }

                    Image {
                        fillMode: Image.PreserveAspectFit
                        anchors.bottomMargin: 10
                        anchors.rightMargin: 10
                        source: "Images/minus.png"
                        anchors.leftMargin: 10
                        anchors.fill: parent
                        anchors.topMargin: 10
                    }
                    border.color: "#400551"
                }


            }
        }

        Row {
            id: row1
            anchors.bottomMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 10
            anchors.bottom: parent.bottom

            Button {
                id: closingButton
                height: 40
                text: qsTr("Cancel")
            }

            Button {
                id: acceptButton
                height: 40
                text: qsTr("OK")
            }
        }

    }
    states: [
        State {
            name: "Vertical"

            PropertyChanges {
                target: inputHours
                anchors.topMargin: 15
            }

            PropertyChanges {
                target: buttonsBar
                height: calendar123.height+7
                width: 190
                anchors.left: calendarFrame.right
                anchors.top: calendarFrame.top
            }

            PropertyChanges {
                target: calendarForDate
                width: calendar123.width+7+buttonsBar.width
                height: calendar123.height+7
            }
        }
    ]
}
