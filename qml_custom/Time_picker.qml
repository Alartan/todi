import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Extras 1.4

Time_pickerForm {
    id: root
    property date chosen_time
    property date chosen_Day
    property date chosen_Hour
    property bool background: true

    state: parent.height>514 ? "":"Vertical"

    signal accepted(date newTime)

    function update_time(time2set) {
        chosen_time = time2set
        chosen_Day = time2set
        chosen_Hour = time2set
        calendar123.selectedDate = chosen_Day
    }

    onVisibleChanged: {
        chosen_Day = chosen_time
        chosen_Hour = chosen_time
    }

    Component.onCompleted: {
        chosen_Day = chosen_time
        chosen_Hour = chosen_time
    }

    closingButton.onClicked: {
        update_time(chosen_time)
        accepted(chosen_time)
    }

    acceptButton.onClicked: {
        var working_time = chosen_Day
        working_time.setHours(chosen_Hour.getHours())
        working_time.setMinutes(chosen_Hour.getMinutes())
        chosen_time = working_time
        accepted(chosen_time)
   }

    calendar123.onClicked: {
        chosen_Day = date
    }

    hoursFieldup.onClicked: {
        var working_time = chosen_Hour;
        working_time.setHours( (chosen_Hour.getHours() + 1)%24 );
        chosen_Hour = working_time;
        hoursField.text = chosen_Hour.getHours()
    }

    hoursFielddown.onClicked: {
        var working_time = chosen_Hour;
        working_time.setHours( (chosen_Hour.getHours() - 1)%24 );
        chosen_Hour = working_time;
        hoursField.text = chosen_Hour.getHours()
    }

    minutesFieldup.onClicked: {
        var working_time = chosen_Hour;
        working_time.setMinutes( (chosen_Hour.getMinutes() + 1)%60 );
        chosen_Hour = working_time;
        minutesField.text = chosen_Hour.getMinutes()
    }

    minutesFielddown.onClicked: {
        var working_time = chosen_Hour;
        working_time.setMinutes( (chosen_Hour.getMinutes() - 1)%60 );
        chosen_Hour = working_time;
        minutesField.text = chosen_Hour.getMinutes()
    }

    hoursField.text: chosen_Hour.getHours()

    minutesField.text: chosen_Hour.getMinutes()
}
