import QtQuick 2.5

WindowCoverForm {
    property Component windowLoader: windowLoader
    property real backOpacity: 0.4
    anchors.fill: parent

    Loader {
        sourceComponent: windowLoader
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }

    Component {
        id: defaultComponent
        Rectangle {

        }
    }
}
