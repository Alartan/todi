import QtQuick 2.4

Item {

    Rectangle {
        anchors.fill: parent
        color: "#000000"
        opacity: backOpacity


        MouseArea {
            hoverEnabled: true
            anchors.fill: parent
        }
    }
}
