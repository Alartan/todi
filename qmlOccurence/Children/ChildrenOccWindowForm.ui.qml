import QtQuick 2.5
import QtQuick.Controls 1.4

Item {
    id: item1
    width: 400
    height: 700
    property alias buttonBirthE: buttonBirthE
    property alias childsPhotoE: childsPhotoE
    property alias textInformationsE: textInformationsE
    property alias buttonConfirmNew: buttonConfirmNew
    property alias textNameE: textNameE
    property alias buttonNew: buttonNew
    property alias buttonEdit: buttonEdit
    property alias buttonDelete: buttonDelete
    property alias buttonConfirm: buttonConfirm
    property alias buttonClose: buttonClose
    property alias buttonCancel: buttonCancel



    Rectangle {
        id: borderRectangle
        gradient: Gradient {
            GradientStop {
                position: 0.33
                color: "#ffffff"
            }

            GradientStop {
                position: 0.721
                color: "#b271c5"
            }
        }
        anchors.bottomMargin: -2
        border.color: "#400551"
        anchors.bottom: barButtons.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        border.width: 2
    }

    Rectangle {
        id: barButtons
        y: 656
        height: 50
        color: "#dfadc1"
        border.color: "#400551"
        border.width: 2
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left

        Column {
            spacing: 5
            anchors.topMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.top: parent.top

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                layoutDirection: Qt.LeftToRight
                spacing: 20

                Button {
                    id: buttonEdit
                    height: 40
                    text: qsTr("Edit")
                }

                Button {
                    id: buttonDelete
                    height: 40
                    text: qsTr("Delete")
                    visible: false
                }

                Button {
                    id: buttonNew
                    height: 40
                    text: qsTr("New")
                    visible: false
                }

                Button {
                    id: buttonConfirmNew
                    height: 40
                    text: qsTr("Confirm")
                    visible: false
                }

                Button {
                    id: buttonCancel
                    height: 40
                    text: qsTr("Edit")
                    visible: false
                }

                Button {
                    id: buttonConfirm
                    height: 40
                    text: qsTr("Confirm")
                    visible: false
                }

                Button {
                    id: buttonClose
                    height: 40
                    text: qsTr("Close")
                }






            }
        }

    }

    ScrollView {
        id: showChild
        visible: true
        anchors.bottomMargin: 2
        anchors.left: borderRectangle.left
        anchors.leftMargin: 2
        anchors.rightMargin: 2
        anchors.right: borderRectangle.right
        anchors.bottom: borderRectangle.bottom
        anchors.topMargin: 2
        anchors.top: borderRectangle.top
        Flickable {
            anchors.fill: parent
            contentHeight: informations.height+informations.anchors.topMargin+informations.anchors.bottomMargin
            boundsBehavior: Flickable.StopAtBounds
            flickableDirection: Flickable.VerticalFlick
            Column {
                id: informations
                anchors.topMargin: spacing
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: 20

                Rectangle {
                    id: barName
                    width: parent.width
                    height: width/2
                    color: "#00000000"

                    Text {
                        id: textName
                        text: qsTr("Name: ")+workingChildOcc.name
                        anchors.verticalCenterOffset: -height
                        anchors.right: parent.right
                        anchors.left: childsPhoto.right
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignHCenter
                        anchors.verticalCenter: parent.verticalCenter
                        font.pixelSize: 20
                    }

                    Text {
                        id: textBirth
                        text: qsTr("Born:  ")+ workingChildOcc.birthDateTime
                        anchors.topMargin: 5
                        horizontalAlignment: Text.AlignHCenter
                        anchors.right: parent.right
                        anchors.left: childsPhoto.right
                        anchors.top: textName.bottom
                        wrapMode: Text.WordWrap
                        font.pixelSize: 15
                    }

                    Image {
                        id: childsPhoto
                        x: 20
                        y: x
                        width: parent.width/2-2*x
                        height: width
                        source: workingChildOcc.childsPhoto
                        cache: false
                    }


                }

                Rectangle {
                    id: barInformations
                    width: parent.width
                    height: textInformationsColumn.childrenRect.height
                    color: "#00000000"

                    Column {
                        id: textInformationsColumn
                        spacing: 8
                        clip: false
                        anchors.rightMargin: 20
                        anchors.bottomMargin: 0
                        anchors.leftMargin: 20
                        anchors.topMargin: 0
                        anchors.fill: parent

                        Text {
                            id: textUser
                            width: parent.width
                            text: qsTr("Parent user:")+workingChildOcc.mainUser
                            wrapMode: Text.WordWrap
                            font.pixelSize: 15
                        }

                        Text {
                            id: textDate
                            width: parent.width
                            text: qsTr("Added to application: ")+workingChildOcc.addedDateTime
                            wrapMode: Text.WordWrap
                            font.pixelSize: 15
                        }

                        Text {
                            id: textInformations
                            width: parent.width
                            text: qsTr("Additional informations: ")+workingChildOcc.additionalInfo
                            wrapMode: Text.WordWrap
                            font.pixelSize: 15
                        }
                    }
                }

            }
        }
    }


    ScrollView {
        id: editChild
        visible: false
        anchors.leftMargin: 2
        anchors.top: rectangle4.bottom
        anchors.bottomMargin: 2
        anchors.rightMargin: 2
        anchors.right: borderRectangle.right
        anchors.left: borderRectangle.left
        Flickable {
            anchors.top: parent.bottom
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            flickableDirection: Flickable.AutoFlickDirection
            boundsBehavior: Flickable.StopAtBounds
            Column {
                id: informationsE
                anchors.top: parent.top

                Rectangle {
                    id: barNamE
                    width: parent.width
                    height: width/2
                    color: "#00000000"
                    Text {
                        id: textName1
                        text: qsTr("Name:")
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 20
                        anchors.verticalCenterOffset: -height
                        anchors.left: childsPhotoE.right
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    Rectangle {
                        color: "#ffffff"
                        border.color: "#dfadc1"
                        border.width: 3
                        anchors.right: parent.right
                        anchors.top: textName1.top
                        anchors.bottom: textName1.bottom
                        anchors.left: textName1.right
                        opacity: 1

                        TextEdit {
                            id: textNameE
                            text: workingChildOcc.name
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.left: parent.left
                            horizontalAlignment: Text.AlignHCenter
                            font.pixelSize: 12
                        }
                    }

                    Text {
                        id: textBirth1
                        text: qsTr("Born: ")
                        anchors.top: textName1.bottom
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 15
                        anchors.left: childsPhotoE.right
                        anchors.topMargin: 5
                    }

                    MouseArea {
                        id: childsPhotoE
                        x: 20
                        y: x
                        width: parent.width/2-2*x
                        height: width
                        hoverEnabled: true

                        Image {
                            source: workingChildOcc.childsPhoto
                            anchors.fill: parent
                            cache: false
                        }

                        Rectangle {
                            id: rectangle1
                            color: "#00000000"
                            border.color: "#b87591"
                            anchors.rightMargin: 2
                            anchors.leftMargin: 2
                            anchors.bottomMargin: 2
                            anchors.topMargin: 2
                            border.width: 3
                            anchors.fill: parent
                        }
                    }

                    Button {
                        id: buttonBirthE
                        anchors.top: textBirth1.top
                        anchors.right: parent.right
                        anchors.left: textBirth1.right
                        anchors.bottom: textBirth1.bottom
                        Text {
                            id: text2
                            text: helpDate
                            anchors.fill: parent
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                    }


                }

                Rectangle {
                    id: barInformationsE
                    width: parent.width
                    height: textInformationsColumnE.childrenRect.height
                    color: "#00000000"
                    Column {
                        id: textInformationsColumnE
                        anchors.leftMargin: 20

                        Text {
                            width: parent.width
                            text: qsTr("Additional informations:")
                            wrapMode: Text.WordWrap
                            font.pixelSize: 15
                        }

                        Rectangle {
                            width: parent.width
                            height: 80
                            color: "#ffffff"
                            border.width: 3
                            border.color: "#dfadc1"
                            TextEdit {
                                id: textInformationsE
                                text: workingChildOcc.additionalInfo
                                anchors.fill: parent
                                font.pixelSize: 12
                                horizontalAlignment: Text.AlignHCenter
                                wrapMode: Text.WordWrap
                            }
                        }
                        anchors.fill: parent
                        anchors.bottomMargin: 0
                        clip: false
                        anchors.rightMargin: 20
                        spacing: 8
                        anchors.topMargin: 0
                    }
                }

                anchors.right: parent.right
                anchors.left: parent.left
                spacing: 20
                anchors.topMargin: spacing
            }
            contentHeight: informationsE.height+informationsE.anchors.topMargin+informationsE.anchors.bottomMargin
        }
        anchors.bottom: borderRectangle.bottom
    }

    Rectangle {
        id: rectangle4
        y: 2
        height: 40
        color: "#ffffff"
        border.color: "#400551"
        border.width: 2
        visible: false
        anchors.left: borderRectangle.left
        anchors.right: borderRectangle.right
        anchors.top: borderRectangle.top

        Text {
            id: text1
            color: "#bca90c"
            text: qsTr("Edit child: ") + workingChildOcc.name
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            anchors.fill: parent
            font.pixelSize: 16
        }
    }

    states: [
        State {
            name: "editState"

            PropertyChanges {
                target: editChild
                visible: true
            }

            PropertyChanges {
                target: buttonEdit
                visible: false
            }

            PropertyChanges {
                target: buttonConfirm
                visible: true
            }

            PropertyChanges {
                target: showChild
                visible: false
            }

            PropertyChanges {
                target: barInformationsE
                height: textInformationsColumnE.childrenRect.height
            }

            PropertyChanges {
                target: textNameE
                x: 223
                y: 128
                opacity: 1
            }

            PropertyChanges {
                target: buttonCancel
                text: qsTr("Cancel")
                visible: true
            }

            PropertyChanges {
                target: buttonClose
                visible: false
            }

            PropertyChanges {
                target: rectangle4
                visible: true
            }

            PropertyChanges {
                target: buttonDelete
                visible: true
            }

            PropertyChanges {
                target: buttonNew
                visible: true
            }
        },
        State {
            name: "newState"

            PropertyChanges {
                target: showChild
                visible: false
            }

            PropertyChanges {
                target: editChild
                visible: true
            }

            PropertyChanges {
                target: textNameE
                text: qsTr("")
            }

            PropertyChanges {
                target: textBirthE
                text: qsTr("")
            }

            PropertyChanges {
                target: buttonEdit
                visible: false
            }

            PropertyChanges {
                target: buttonConfirm
                visible: false
            }

            PropertyChanges {
                target: buttonClose
                visible: false
            }

            PropertyChanges {
                target: buttonCancel
                text: qsTr("Cancel")
                visible: true
            }

            PropertyChanges {
                target: buttonConfirmNew
                visible: true
            }

            PropertyChanges {
                target: rectangle4
                visible: true
            }

            PropertyChanges {
                target: textInformationsE
                text: ""
                anchors.rightMargin: 3
                anchors.leftMargin: 3
                anchors.bottomMargin: 3
                anchors.topMargin: 3
                cursorVisible: true
            }

            PropertyChanges {
                target: text1
                text: "New child"
            }

            PropertyChanges {
                target: text2
                text: helpDate
            }
        },
        State {
            name: "addFirst"

            PropertyChanges {
                target: buttonEdit
                visible: false
            }

            PropertyChanges {
                target: buttonConfirmNew
                visible: true
            }

            PropertyChanges {
                target: buttonClose
                visible: false
            }

            PropertyChanges {
                target: showChild
                visible: false
            }

            PropertyChanges {
                target: editChild
                visible: true
            }

            PropertyChanges {
                target: text1
                text: qsTr("Add at least one child")
            }

            PropertyChanges {
                target: rectangle4
                visible: true
            }

            PropertyChanges {
                target: textNameE
                text: ""
            }

            PropertyChanges {
                target: textInformationsE
                text: qsTr("")
            }
        }
    ]

}
