import QtQuick 2.5
import QtQuick.Controls 1.4

Item {
    width: 400
    height: 400
    property alias buttonClose: buttonClose
    property alias listForModel: listForModel

    Rectangle {
        id: rectangle1
        color: "#ffffff"
        anchors.fill: parent
        border.width: 2

        Rectangle {
            id: rectangle2
            height: 48
            color: "#ffffff"
            border.width: 2
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right

            Button {
                id: buttonClose
                height: 40
                text: qsTr("Close")
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }

        ScrollView {
            anchors.bottom: rectangle2.top
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.top: parent.top

            ListView {
                id: listForModel
                anchors.fill: parent
                anchors.bottom: rectangle2.top
                model: ["kalafio", "kaszka", "kawalec"]
                delegate: Item {
                    height: 40
                    property alias childMouseArea: childMouseArea
                    width: listForModel.width

                    Rectangle {
                        height: 40
                        color: "#dfadc1"
                        border.color: "#400551"
                        border.width: 2
                        anchors.left: parent.left
                        anchors.right: parent.right

                        Text {
                            text: modelData
                            anchors.fill: parent
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            font.pixelSize: 12
                        }

                        MouseArea {
                            id: childMouseArea
                            anchors.fill: parent
                        }
                    }
                }
            }
        }
    }
}
