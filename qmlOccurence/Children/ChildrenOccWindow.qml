import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import alart.liboccurence 1.0
import "qrc:/qml_custom"

ChildrenOccWindowForm {
    property ChildOcc workingChildOcc: defaultChildOcc
    property var childOccList: ["Jasiek", "Stefan", "Zosia", "Kundzia"]
    property var photoBefore

    signal noChild()
    onNoChild: state = "addFirst"

    ChildOcc {
        id: defaultChildOcc
        name: "Jasiek"
        birthDateTime: new Date
        childSex: false
        additionalInfo: "Nic ciekawego tutaj nie będzie napisane"
        childsPhoto: "qrc:/qml_custom/Images/Tux.png"
    }

    function reset() {
        workingChildOcc.setChildsPhoto(photoBefore)
        textNameE.text = workingChildOcc.name
        textInformationsE.text = workingChildOcc.additionalInfo
        state = ""
    }

    buttonEdit.onClicked: state = "editState"
    buttonCancel.onClicked: {
//        measurementsView.cancelMeasurements()
        reset()
    }
    buttonConfirm.onClicked: {
        workingChildOcc.setName(textNameE.text)
        workingChildOcc.setAdditionalInfo(textInformationsE.text)
        workingChildOcc.setBirthDateTime(helpDate)
        workingChildOcc.childEditConfirmed()
        reset()
    }
    buttonDelete.onClicked: {
        workingChildOcc.childDeleteConfirmed();
        state = "";
        buttonClose.clicked()
    }

    buttonNew.onClicked: state = "newState"
    buttonConfirmNew.onClicked: {
        workingChildOcc.setName(textNameE.text)
        workingChildOcc.setAdditionalInfo(textInformationsE.text)
        workingChildOcc.setBirthDateTime(helpDate)
        workingChildOcc.childNewConfirmed();
        reset()
        buttonClose.clicked()
    }


    FileDialog {
        id: photoFileDialog
        // @disable-check M16
        title: qsTr("Chose photo for your child")
        // @disable-check M16
        onAccepted: {
            photoBefore = workingChildOcc.childsPhoto
            workingChildOcc.setChildsPhoto(fileUrl)
            console.log("You chose: " + fileDialog.fileUrls)
        }
        // @disable-check M16
        nameFilters: [ "Image files (*.jpg *jpeg *.png)", "All files (*)" ]
        // @disable-check M16
        visible: false
    }
    childsPhotoE.onClicked: photoFileDialog.visible = true
    childsPhotoE.onEntered: childsPhotoE.opacity = 0.8
    childsPhotoE.onExited: childsPhotoE.opacity = 1

    property date helpDate: workingChildOcc.birthDateTime;
    onStateChanged: {
        if (state === "newState") helpDate = new Date
        else helpDate = workingChildOcc.birthDateTime
    }
    WindowCover {
        id: birthTimePicker
        visible: false
        z: 200
        windowLoader: Component {
            Time_picker {
                chosen_time: helpDate
                background: true
                onAccepted: {
                    helpDate = newTime
                    birthTimePicker.visible = false
                }
            }
        }
    }
    buttonBirthE.onClicked: birthTimePicker.visible = true
}
