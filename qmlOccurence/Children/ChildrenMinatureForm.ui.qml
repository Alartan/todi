import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtGraphicalEffects 1.0

Item {
    id: item1
    property alias childsName: childsName
    property alias childsListButton: childsListButton
    property alias childMainButton: childMainButton
    property alias childsImage: childsImage
    width: childsListButton.height/2+rectangle1.width+childsListButton.height
    height: 80

    MouseArea {
        id: childMainButton
        width: height/2+rectangle1.width
        anchors.horizontalCenterOffset: -childsListButton.width/2
        anchors.horizontalCenter: parent.horizontalCenter
        hoverEnabled: true
        z: 20
        anchors.bottom: rectangle2.top
        anchors.top: parent.top

        Rectangle {
            id: rectChildImg
            width: height
            z: 10
            anchors.bottomMargin: 2
            anchors.topMargin: 2
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.top: parent.top
            color: "transparent"
            radius: height/4
            border.color: "#400551"
            border.width:  (childMainButton.opacity === 1 ) ? 2 : 0



            Rectangle {
                id: childsImageMask
                x: 0
                y: 2
                radius: height/4
                anchors.fill: childsImage
                smooth: true
                visible: false
            }

            Image {
                id: childsImage
                anchors.topMargin: 2
                anchors.rightMargin: 2
                anchors.leftMargin: 2
                anchors.bottomMargin: 2
                anchors.fill: parent
                source: "../Images/kolec.jpeg"
                smooth: true
                visible: false
            }

            OpacityMask {
                clip: true
                anchors.fill: childsImage
                source: childsImage
                maskSource: childsImageMask
            }
        }

        Rectangle {
            id: rectangle1
            width: childrenRect.width+rectChildImg.width/2+(height-childsName.font.pixelSize)
            border.color: "#400551"
            anchors.bottomMargin: parent.height/4
            anchors.topMargin: parent.height/4
            border.width: (childMainButton.opacity === 1 ) ? 2 : 0
            z: -3
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            Text {
                id: childsName
                x: rectChildImg.width/2+(parent.height-font.pixelSize)/2
                text: qsTr("Name can be very long")
                font.pixelSize: 16
                anchors.bottom: parent.bottom
                anchors.top: parent.top
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignRight
            }
        }
    }

    Button {
        id: childsListButton
        width: height
        text: qsTr("Button")
        anchors.left: childMainButton.right
        anchors.bottom: rectangle2.top
        anchors.top: parent.top
    }

    Rectangle {
        id: rectangle2
        width: childsName1.width+6
        height: 0
        color: "#00000000"
        clip: true
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        z: -3
        border.width: (childMainButton.opacity === 1 ) ? 2 : 0
        border.color: "#400551"
        Text {
            id: childsName1
            text: qsTr("Name can be very long")
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 16
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }
    states: [
        State {
            name: "Small"

            PropertyChanges {
                target: rectangle1
                visible: false
            }

            PropertyChanges {
                target: rectangle2
                height: childsName1.height+6
            }

            PropertyChanges {
                target: childMainButton
                width: height
            }

            PropertyChanges {
                target: item1
                width: rectangle2.width
            }
        }
    ]
}
