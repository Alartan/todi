import QtQuick 2.5
import QtQuick.Controls 1.4
import alart.liboccurence 1.0

ChildrenListForm {
    property var childrenList: ["kalafio", "kaszka", "kawalec"]
    property int maxHeight: 400
    signal childChosen(var childIndex)

    height: listForModel.model.length*40 > maxHeight ? maxHeight : listForModel.model.length*40+buttonClose.height+8

    listForModel.model: childrenList
    listForModel.delegate:
    Item {
        id: childDelegate
        height: 40
        property alias childMouseArea: childMouseArea
        width: listForModel.width

        Rectangle {
            height: 40
            color: "#dfadc1"
            border.color: "#400551"
            border.width: 2
            anchors.left: parent.left
            anchors.right: parent.right

            Text {
                text: modelData
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 12
            }

            MouseArea {
                id: childMouseArea
                anchors.fill: parent
                onClicked: childChosen(index)
            }
        }
    }
}
