import QtQuick 2.5
import QtQuick.Controls 1.4
import alart.liboccurence 1.0

ChildrenMinatureForm {
    property ChildOcc workingChildOcc: ChildOcc {
        name:"Default child"
        birthDateTime: new Date
        childSex: false
        additionalInfo: "Nic ciekawego tutaj nie będzie napisane"
        childsPhoto:"qrc:/Images/kolec.jpeg"
    }

    childsImage.source: workingChildOcc.childsPhoto
    childsName.text: workingChildOcc.name

    childMainButton.onEntered: {
        childMainButton.opacity = 0.8
    }
    childMainButton.onExited: childMainButton.opacity = 1

    Connections {
        target: childMainButton
        onClicked: childClicked();
    }

    Connections {
        target: childsListButton
        onClicked: childsListClicked()
    }

    signal childClicked ();
    signal childsListClicked ();
}
