import QtQuick 2.4
import QtQuick.Controls 1.5

Item {
    width: 400
    height: 400
    property alias button1: button1

    Rectangle {
        id: rectangle1
        color: "#ffffff"
        anchors.fill: parent

        Button {
            id: button1
            x: 183
            y: 183
            text: qsTr("Hide")
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
