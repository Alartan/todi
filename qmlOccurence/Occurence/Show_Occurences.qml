import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Extras 1.4
import alart.liboccurence 1.0
import "qrc:/qmlOccurence/"
import "qrc:/qmlOccurence/Occurence/"
import "qrc:/qml_custom/"

Show_OccurencesForm {
    aggregateList.delegate: listInAggregate

    clip: true

    property int componentHeight: Math.max(parent.height/7 , 40)
    property int componentBorder: 1
    property int inListMargin: 40

    Component {
        id: listInAggregate
        Item {
            id: item1
            width: aggregateList.width
            height: componentHeight

            Rectangle {
                id: dayAggshow
                width: hoursText.anchors.leftMargin
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                color: "#cdaadf"
                border.width: componentBorder
                Text {
                    text: Qt.formatDateTime(agregateDay, "dd MMM yyyy")
                    font.pointSize: 14
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.bold: false
                    anchors.verticalCenter: parent.verticalCenter
                }
            }

            Rectangle {
                color: "#00ffffff"
                anchors.rightMargin: -componentBorder
                anchors.leftMargin: -componentBorder
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.left: dayAggshow.right
                border.width: componentBorder
            }

            Item {
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.left: dayAggshow.right
                clip: true
                ListView {
                    id: show_occurenciesList
                    height: componentHeight-2*componentBorder
                    anchors.left: parent.left
                    anchors.leftMargin: inListMargin
                    anchors.top: parent.top
                    anchors.topMargin: componentBorder
                    anchors.right: parent.right
                    anchors.rightMargin: inListMargin
                    focus: true
                    delegate: OccurenceShown {
                        listWidth: show_occurenciesList.width}
                    model:list_shown_Occ
                }

                MouseArea {
                    anchors.fill: parent
                    propagateComposedEvents: true
                    onDoubleClicked: {
                        var newTime = new Date(Math.ceil((mouseX - inListMargin)*3600*24*1000/(width-2*inListMargin))+agregateDay.getTime())
                        console.log(newTime)
                        console.log("double")
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    propagateComposedEvents: true
                    acceptedButtons: Qt.RightButton
                    onClicked: {
                        var newTime = new Date(Math.ceil((mouseX - inListMargin)*3600*24*1000/(width-2*inListMargin))+agregateDay.getTime())
                        addOccurenceForm.setDefTime(newTime)
                        addOccurenceForm.state = ""
                        addOccurenceCover.visible = true
                    }
                }
            }
        }
    }
}
