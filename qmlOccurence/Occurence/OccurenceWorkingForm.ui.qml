import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import alart.liboccurence 1.0

Item {
    id: addoccMainItem
    property alias occurenciesList: occurenciesList
    property alias occ_featuresList: occ_featuresList
    property alias occurenceCancel: occurenceCancel
    property alias occurenceAdd: occurenceAdd
    property alias occurenciesContainer: occurenciesContainer
    width: 350
    property alias occurenceDelete: occurenceDelete
    property alias occurenceUpdate: occurenceUpdate

    Rectangle {
        id: barButtons
        height: 60
        color: "#dfadc1"
        anchors.right: parent.right
        border.color: "#400551"
        border.width: 2
        anchors.left: parent.left
        anchors.bottom: parent.bottom

        Row {
            id: row1
            anchors.topMargin: 10
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.leftMargin: 10
            spacing: 10

            Button {
                id: occurenceUpdate
                width:  (parent.width - 40)/3
                height: 40
                text: qsTr("Save")
                visible: false
            }

            Button {
                id: occurenceCancel
                width:  (parent.width - 40)/3
                height: 40
                text: qsTr("Cancel")
            }

            Button {
                id: occurenceAdd
                width:  (parent.width - 40)/3
                height: 40
                text: qsTr("Add")
                visible: false

            }

            Button {
                id: occurenceDelete
                width:  (parent.width - 40)/3
                height: 40
                text: qsTr("Delete")
                visible: false
            }

        }

    }

    Item {
        id: item1
        anchors.bottom: barButtons.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top

        Rectangle {
            id: occurenciesContainer
            width: parent.width
            color: "#ffffff"
            clip: true
            anchors.left: parent.left
            anchors.top: parent.top
            border.color: "#400551"
            border.width: 2
            anchors.bottom: parent.bottom

            ScrollView {
                anchors.leftMargin: 2
                anchors.rightMargin: 2
                anchors.bottomMargin: 2
                anchors.topMargin: 2
                anchors.fill: parent

                ListView {
                    id: occurenciesList
                    anchors.fill: parent
                    delegate: occurenceDelegate
                    focus: true
                    currentIndex: -1
                }

            }
        }

        Rectangle {
            id: rectangle3
            color: "#ffffff"
            anchors.right: parent.right
            border.color: "#400551"
            border.width: 2
            anchors.left: occurenciesContainer.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom

            ScrollView {
                anchors.rightMargin: 2
                anchors.leftMargin: 2
                anchors.bottomMargin: 2
                anchors.topMargin: 2
                anchors.fill: parent

                ListView {
                    id: occ_featuresList
                    anchors.fill: parent
                    clip: true
                    focus: true
                }
            }
        }

    }

    Rectangle {
        id: barTitle
        x: 7
        y: 3
        height: 40
        color: "#dfadc1"
        visible: false
        anchors.right: parent.right
        anchors.bottom: item1.top
        border.width: 2
        anchors.left: parent.left
        border.color: "#400551"

        Text {
            id: text1
            text: qsTr(workingOccurence.name)
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 16
        }
    }
    states: [
        State {
            name: "addOcc"

            PropertyChanges {
                target: occurenceAdd
                visible: true
            }
        },
        State {
            name: "editOcc"

            PropertyChanges {
                target: occurenceDelete
                visible: true
            }

            PropertyChanges {
                target: occurenceUpdate
                visible: true
            }

            PropertyChanges {
                target: occurenciesContainer
                width: 0
            }

            PropertyChanges {
                target: barTitle
                visible: true
            }

            PropertyChanges {
                target: item1
                anchors.topMargin: barTitle.height
            }
        },
        State {
            name: "addFeatures"

            PropertyChanges {
                target: occurenciesContainer
                width: parent.width*0.35/0.75
            }

            PropertyChanges {
                target: occurenceAdd
                visible: true
            }
        }
    ]
}

