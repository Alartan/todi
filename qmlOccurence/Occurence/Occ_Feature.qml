import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import alart.liboccurence 1.0
import "qrc:/qml_custom/"

/* Component is supposed to be bound to Feature
  But in this case no feature is explicitely defined.

*/

Occ_FeatureForm {
    signal acceptedFeatures();
    Text {
        id: featureName
        text: qsTr(name) + ":"
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 11
    }
    ComboBox {
        anchors.left: parent.left
        anchors.right: parent.right
        model: ocOpOptions
        property var modelIndex: index
        anchors.rightMargin: 4
        anchors.leftMargin: 4
        anchors.verticalCenter: parent.verticalCenter
        anchors.top: featureName.bottom
        visible: type==="list" ? true : false
        onActivated: defaultValue = textAt(index)
        Component.onCompleted: {
            if (type === "list") {
                if (defaultValue != "") currentIndex = find(defaultValue)
                }
        }
    }
    Button {
        text: type==="date" ? Qt.formatDateTime(defaultValue, "dd MMMM yyyy hh:mm") : ""
        anchors.rightMargin: 4
        anchors.leftMargin: 4
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: featureName.bottom
        visible: type==="date" ? true : false
        signal timeChanged(date newTime)
        Component.onCompleted: {
            if (type === "date" && defaultValue === "") {
                defaultValue = new Date(new Date().setSeconds(0, 0));
            }
        }
        onClicked : {
//            @TODO: global object
            coverRect.windowLoader = timer
            coverRect.visible = true
        }
        onTimeChanged: {
            defaultValue = newTime
        }
    }
    Rectangle {
        id: rectangle1
        anchors.rightMargin: 2
        anchors.leftMargin: 2
        anchors.bottomMargin: 2
        anchors.topMargin: 2
        border.color: "#400551"
        border.width: 1
        anchors.top: featureName.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        visible: type==="text" ? true : false
        radius: 5
        TextEdit {
            anchors.rightMargin: 2
            anchors.leftMargin: 2
            anchors.bottomMargin: 2
            anchors.topMargin: 2
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            wrapMode: TextInput.Wrap
            signal editingFinished()
            Component.onCompleted: {
                if (type === "text") {
                    acceptedFeatures.connect(editingFinished)
                    if (defaultValue != "") text = defaultValue
                }
            }
            onEditingFinished: {
                defaultValue = text
            }
        }
    }

    Component {
        id: timer
        Time_picker {
            chosen_time: defaultValue
            background: true
            onAccepted: {
                defaultValue = newTime
                coverRect.visible = false
            }
            Component.onCompleted: console.log(defaultValue)
        }
    }
}
