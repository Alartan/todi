import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Extras 1.4
import alart.liboccurence 1.0
import "qrc:/qml_custom/"

OccurenceWorkingForm {

    signal accepted()
    signal closed()
    property int rootWidth;
    property int rootHeight;
    property Robocze_Wydarzenie workingOccurence;
    // @disable-check M311
    property var defTime: "no date"
    width: state === "editOcc" ? Math.max(rootWidth*0.4, 150) : Math.max(rootWidth*0.35, 120)
    height:Math.min(occurenciesList.model.length*50+60, rootHeight*0.8)

    occ_featuresList.delegate: Occ_Feature {
        width: parent.width; height:  type === "void" ? 0 : 80
        visible: type === "void" ? false : true
        Component.onCompleted: {
            accepted.connect(acceptedFeatures)
        }
    }

    occ_featuresList.model: workingOccurence.listProper

    Component {
        id: occurenceDelegate
        Item {
            width: parent.width; height: 50
            Rectangle {
                id: rectangle1
                color:  occurenciesList.currentIndex === index ? "#dfadc1" : "#ffffff"
                border.color: "#400551"
                anchors.fill: parent

                MouseArea {
                    id: mouseArea1
                    anchors.fill: parent
                    onClicked: {
                        if (defTime instanceof Date) {
                            console.log(defTime)
                            Lista_Wydarzen.setWorkName(name, defTime) // to jest odwołanie do obiektu globalnego
                        }
                        else Lista_Wydarzen.setWorkName(name)
                        chooseOccurencetoShow()
                        occurenciesList.currentIndex = index
                    }
                }

                Text {
                    id: text1
                    text: qsTr(name)
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 14
                }
            }
        }
    }

    MessageDialog {
        id: messageDialog
        // @disable-check M16
        title: "Information from Infant's Diary"
        // @disable-check M16
        text: "It's so cool that you are using Qt Quick."
        // @disable-check M16
        icon: StandardIcon.Information
        // @disable-check M16
        onAccepted: {
            close()
        }
    }

    occurenceCancel.onClicked: {
        close()
    }

    occurenceAdd.onClicked: {
        accepted()
        var error_i = Lista_Wydarzen.sentOccurenceToServer()
        if (error_i === -1) {
            messageDialog.text = qsTr("There is no occurence to add, is advised to report it to creator.")
            messageDialog.open()
            return
        }
        if (error_i === -2) {
            messageDialog.text = qsTr("Created occurence ended before start, is advised to fix that.")
            messageDialog.open()
            return
        }
        close()
    }

    occurenceUpdate.onClicked: {
        accepted()
        var error_i = Lista_Wydarzen.loadWorkingtoShowOcc()
        if (error_i === -1) {
            messageDialog.text = qsTr("There is no occurence to add, is advised to report it to creator.")
            messageDialog.open()
            return
        }
        if (error_i === -2) {
            messageDialog.text = qsTr("Created occurence ended before start, is advised to fix that.")
            messageDialog.open()
            return
        }
        close()
    }

    occurenceDelete.onClicked: {
        Lista_Wydarzen.deleteShowOcc()
        close()
    }

    function chooseOccurencetoShow() {
            width = Math.max(rootWidth*0.75, 300)
            state = "addFeatures"
        }

    function setDefTime(newTime) {
        if (newTime instanceof Date) {
            defTime = newTime
            return true
        }
        else return false
    }

    function close() {
        state = ""
        width = state === "editOcc" ? Math.max(rootWidth*0.4, 150) : Math.max(rootWidth*0.35, 120)
        occurenciesList.currentIndex = -1
        defTime = "no date"
        closed()
    }
}

