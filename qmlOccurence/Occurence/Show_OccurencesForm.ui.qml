import QtQuick 2.5
import QtQuick.Controls 1.4
import alart.liboccurence 1.0

Item {
    width: 400
    height: 400
    property alias rowAxis: rowAxis
    property alias hoursText: hoursText
    //    property alias show_occurenciesList: show_occurenciesList
    property alias aggregateList: aggregateList
    id: showoccMainItem

    Rectangle {
        id: hoursAxis
        height: 20
        color: "#ffffff"
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right

        Rectangle {
            id: hoursText
            border.width: componentBorder
            anchors.leftMargin: 120
            anchors.fill: parent

            Row {
                id: rowAxis
                anchors.leftMargin: inListMargin-text1.width/2
                anchors.rightMargin: inListMargin-text1.width/2
                anchors.fill: parent
                spacing: (width-text1.width*5)/4

                Text {
                    id: text1
                    text: qsTr("00:00")
                    font.pixelSize: 12
                }

                Text {
                    id: text2
                    text: qsTr("06:00")
                    font.pixelSize: 12
                }

                Text {
                    id: text3
                    text: qsTr("12:00")
                    font.pixelSize: 12
                }

                Text {
                    id: text4
                    text: qsTr("18:00")
                    font.pixelSize: 12
                }

                Text {
                    id: text5
                    text: qsTr("00:00")
                    font.pixelSize: 12
                }
            }
        }
    }

    ListView {
        id: aggregateList
        z: 1
        anchors.top: hoursAxis.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        clip: true
        model: ListModel {
            ListElement {
                agregateDay: "Grey"
            }
            ListElement {
                agregateDay: "Red"
            }
        }
        delegate: Item {
            width: showoccMainItem.width
            height: 40
            Rectangle {
                id: aggregateDayRectangle
                width: hoursText.anchors.leftMargin
                color: "green"
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                Text {
                    text: agregateDay
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.bold: true
                    anchors.verticalCenter: parent.verticalCenter
                }
            }

            Rectangle {
                id: rectangle1
                color: "#00ffffff"
                border.width: 0
                clip: true
                anchors.left: aggregateDayRectangle.right
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.top: parent.top
                anchors.leftMargin: 0

                ListView {
                    id: show_occurenciesList
                    clip: true
                    anchors.rightMargin: rowAxis.anchors.rightMargin
                    anchors.leftMargin: rowAxis.anchors.leftMargin
                    anchors.fill: parent
                    focus: true
                    delegate:Item {
                        id: item1
                        width: show_occurenciesList.width

                        Rectangle {
                            id: pointTimeStart
                            x: parent.width*(inttimeStart-8000) / (16000-8000)
                            width: 20
                            height: width
                            color: "#479cc6"
                            radius: width/2
                        }

                        Rectangle {
                            id: pointTimeEnd
                            x: pointTimeStart.x+lineOccurence.width
                            width: pointTimeStart.width
                            height: width
                            color: "#d31111"
                            radius: width/2
                            anchors.verticalCenter: pointTimeStart.verticalCenter
                        }

                        Rectangle {
                            id: lineOccurence
                            x: pointTimeStart.x+pointTimeStart.width/2
                            y: pointTimeStart.y
                            width: parent.width*(inttimeEnd - inttimeStart) / (16000-8000)
                            height: 18
                            color: "#ade340"
                            z: -1
                            anchors.verticalCenter: pointTimeStart.verticalCenter
                        }
                    }
                    model:    ListModel {
                        ListElement {
                            name: "Bill Smith"
                            inttimeStart: 10000
                            inttimeEnd: 13000
                        }
                        ListElement {
                            name: "John Brown"
                            inttimeStart: 9000
                            inttimeEnd: 12000
                        }
                        ListElement {
                            name: "Sam Wise"
                            inttimeStart: 14000
                            inttimeEnd: 15000
                        }
                    }
                }
            }
        }
    }

    Item {
        id: hourLines
        anchors.leftMargin: hoursText.anchors.leftMargin
        anchors.top: hoursAxis.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left

        Row {
            id: rowAxis1
            anchors.leftMargin: inListMargin
            anchors.rightMargin: inListMargin
            anchors.fill: parent
            spacing: (width-5)/4

            Rectangle {
                id: rectangle2
                width: 1
                color: "#000000"
                anchors.bottom: parent.bottom
                anchors.top: parent.top
            }

            Rectangle {
                id: rectangle3
                width: 1
                color: "#000000"
                anchors.top: parent.top
                anchors.bottom: parent.bottom
            }

            Rectangle {
                id: rectangle4
                width: 1
                color: "#000000"
                anchors.top: parent.top
                anchors.bottom: parent.bottom
            }

            Rectangle {
                id: rectangle5
                width: 1
                color: "#000000"
                anchors.top: parent.top
                anchors.bottom: parent.bottom
            }

            Rectangle {
                id: rectangle6
                width: 1
                color: "#000000"
                anchors.top: parent.top
                anchors.bottom: parent.bottom
            }
        }
    }

}
