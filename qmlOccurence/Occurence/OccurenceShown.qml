import QtQuick 2.5
import QtQuick.Controls 1.4
import alart.liboccurence 1.0

OccurenceShownForm {
    property int listWidth: 100
    width: listWidth
    z: shownOccurenceLoader.item.z

    Component.onCompleted: {
        if(name === "Meal") {
            levelDisplay = 0;
            return;
        }
        if(name === "Urination") {
            levelDisplay = 6;
            return;
        }
        if(name === "Defecation") {
            levelDisplay = 6;
            return;
        }
        if(name === "Cry") {
            levelDisplay = 1;
            return;
        }
        if(name === "Play") {
            levelDisplay = 0;
            return;
        }
        if(name === "Other") {
            levelDisplay = 0;
            return;
        }
        if(name === "Sleep") {
            levelDisplay = 0;
            return;
        }
        else levelDisplay = -1;
    }

    Loader {
        id: shownOccurenceLoader
        sourceComponent: switch(name) {
                        case "Sleep": return sleep
                        case "Play": return play
                        case "Meal": return meal
                        case "Other": return other
                        case "Cry": return cry
                        case "Urination": return urination
                        case "Defecation": return defecation
                    }
    }

    Component {
        id: sleep
        Rectangle {
            id: rectangle2
            x: listWidth * (inttimeStart) / (3600*24)
            y: (levelDisplay === -1) ? (componentHeight-2*componentBorder)/5 : levelDisplay/2*(componentHeight-2*componentBorder)/5
            height: (componentHeight-2*componentBorder)
            width: Math.max(height, listWidth * 300 / (3600*24), listWidth * (inttimeEnd - inttimeStart) / (3600*24))
            color: "#33000000"
            z: -1

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Lista_Wydarzen.loadShowtoWorkingOcc(occID, dayOcc)
                    addOccurenceForm.state = "editOcc"
                    addOccurenceCover.visible = true
                }
            }

            Rectangle {
                id: rectangle1
                width: height
                rotation: 270
                gradient: Gradient {
                    GradientStop {
                        position: 1
                        color: "transparent"
                    }

                    GradientStop {
                        position: 0
                        color: "#000000"
                    }
                }
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
            }

            Image {
                id: image1
                width: height
                height: parent.height/4
                anchors.left: parent.left
                anchors.top: parent.top
                source: "Images/sleepIcon.png"
            }
        }
    }

    Component {
        id: play
        Rectangle {
            id: rectangle2
            x: listWidth * (inttimeStart) / (3600*24)
            y: (levelDisplay === -1) ? (componentHeight-2*componentBorder)/5 : levelDisplay/2*(componentHeight-2*componentBorder)/5
            height: (componentHeight-2*componentBorder)
            width: Math.max(height, listWidth * 300 / (3600*24), listWidth * (inttimeEnd - inttimeStart) / (3600*24))
            color: "#3323cb19"
            z: -1

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Lista_Wydarzen.loadShowtoWorkingOcc(occID, dayOcc)
                    addOccurenceForm.state = "editOcc"
                    addOccurenceCover.visible = true
                }
            }

            Rectangle {
                id: rectangle1
                width: height
                rotation: 270
                gradient: Gradient {
                    GradientStop {
                        position: 1
                        color: "transparent"
                    }

                    GradientStop {
                        position: 0
                        color: "#23cb19"
                    }
                }
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
            }

            Image {
                id: image1
                width: height
                height: parent.height/4
                anchors.left: parent.left
                anchors.top: parent.top
                source: "Images/sleepIcon.png"
            }
        }
    }

    Component {
        id: meal
        Rectangle {
            x: listWidth * (inttimeStart) / (3600*24)
            y: (levelDisplay === -1) ? (componentHeight-2*componentBorder)/5 : levelDisplay/2*(componentHeight-2*componentBorder)/5
            height: (componentHeight-2*componentBorder)/5*3
            width: Math.max(height, listWidth * 300 / (3600*24), listWidth * (inttimeEnd - inttimeStart) / (3600*24))
            color: "#8b46f0"
            z: 1
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Lista_Wydarzen.loadShowtoWorkingOcc(occID, dayOcc)
                    addOccurenceForm.state = "editOcc"
                    addOccurenceCover.visible = true
                }
            }
        }
    }

    Component {
        id: other
        Rectangle {
            x: listWidth * (inttimeStart) / (3600*24)
            y: (levelDisplay === -1) ? (componentHeight-2*componentBorder)/5 : levelDisplay/2*(componentHeight-2*componentBorder)/5
            height: (componentHeight-2*componentBorder)/5*3
            width: Math.max(height, listWidth * 300 / (3600*24), listWidth * (inttimeEnd - inttimeStart) / (3600*24))
            color: "#c444af"
            z: 1
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Lista_Wydarzen.loadShowtoWorkingOcc(occID, dayOcc)
                    addOccurenceForm.state = "editOcc"
                    addOccurenceCover.visible = true
                }
            }
        }
    }

    Component {
        id: cry
        Rectangle {
            x: listWidth * (inttimeStart) / (3600*24)
            y: (levelDisplay === -1) ? (componentHeight-2*componentBorder)/5 : levelDisplay/2*(componentHeight-2*componentBorder)/5
            height: (componentHeight-2*componentBorder)/5*2
            width: Math.max(height, listWidth * 300 / (3600*24), listWidth * (inttimeEnd - inttimeStart) / (3600*24))
            radius: height/2
            color: "#3ba0ed"
            z: 5
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Lista_Wydarzen.loadShowtoWorkingOcc(occID, dayOcc)
                    addOccurenceForm.state = "editOcc"
                    addOccurenceCover.visible = true
                }
            }
        }
    }

    Component {
        id: urination
        Rectangle {
            x: listWidth * (inttimeStart) / (3600*24)
            y: (levelDisplay === -1) ? (componentHeight-2*componentBorder)/5 : levelDisplay/2*(componentHeight-2*componentBorder)/5
            height: (componentHeight-2*componentBorder)/5*2
            width: Math.max(height, listWidth * 300 / (3600*24), listWidth * (inttimeEnd - inttimeStart) / (3600*24))
            radius: height/2
            color: "yellow"
            z: 10
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Lista_Wydarzen.loadShowtoWorkingOcc(occID, dayOcc)
                    addOccurenceForm.state = "editOcc"
                    addOccurenceCover.visible = true
                }
            }
        }
    }

    Component {
        id: defecation
        Rectangle {
            x: listWidth * (inttimeStart) / (3600*24)
            y: (levelDisplay === -1) ? (componentHeight-2*componentBorder)/5 : levelDisplay/2*(componentHeight-2*componentBorder)/5
            height: (componentHeight-2*componentBorder)/5*2
            width: Math.max(height, listWidth * 300 / (3600*24), listWidth * (inttimeEnd - inttimeStart) / (3600*24))
            radius: height/2
            color: "#955738"
            z: 10
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Lista_Wydarzen.loadShowtoWorkingOcc(occID, dayOcc)
                    addOccurenceForm.state = "editOcc"
                    addOccurenceCover.visible = true
                }
            }
        }
    }
}
