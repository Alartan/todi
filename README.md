# README #

ToDi application is an application that stores what's happened in child's life. It's supposed to help parent control health and development of their kid.

### Stack ###

* This is a desktop app written in Qt (C++) + QML (JS).
* RapidXML parser
* Informations are stored in sqlite database
* It works on Linux, on Windows there were some errors with initialization of database and it never run successfully on Android/iOS
* It's unpolished and there are no tests unfortunately.

### How do I get set up? ###

* Download and install QT Creator
* clone this repo
* install this library: sudo apt-get install libglu1-mesa-dev
* Build an application

### Further development ###
It's frozen project. I was young and had limited resources and first tests indicated that parents are very busy with children so apparently have no time to use this app. The idea may be suitable for someone nonethanless.

It's open source, you can use it but may be nice to mention me somewhere ;)