import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import "qrc:/qmlOccurence/"
import "qrc:/qmlOccurence/Children/"
import "qrc:/qmlOccurence/Occurence/"
import "qrc:/qmlOccurence/User/"
import "qrc:/qml_custom/"
import "qrc:/mainForm/"

ApplicationWindow {
    id: rootWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Dane Dziecka")

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }
    Item {
        anchors.fill: parent
        id: root


        Component.onCompleted: {
            if(!Lista_Wydarzen.isAnyChild()) addFirstChild()
        }
        function addFirstChild() {
            coverRectangle.windowLoader = childView
            coverRectangle.visible = true
            console.log ("sprawdzam dziecko")
        }

        TopBar {
            id: topBarControl
            height: 0.1*parent.height
            anchors.right: parent.right
            anchors.top: parent.top
            width:parent.width

            startTime.text: Qt.formatDateTime(Lista_Wydarzen.startDayView, "dd MMMM yyyy")
            startTime.onClicked: {
                coverRectangle.windowLoader = starttimePicker
                coverRectangle.visible = true
            }
            endTime.text: Qt.formatDateTime(Lista_Wydarzen.endDayView, "dd MMMM yyyy")
            endTime.onClicked: {
                coverRectangle.windowLoader = endtimePicker
                coverRectangle.visible = true
            }
            MessageDialog {
                id: messageDialog
                title: "Information from Infant's Diary"
                text: "It's so cool that you are using Qt Quick."
                icon: StandardIcon.Information
                onAccepted: {
                    close()
                }
            }

            displayChild.workingChildOcc: Lista_Wydarzen.workingChild
            displayChild.onChildClicked: {
                coverRectangle.windowLoader = childView
                coverRectangle.visible = true
            }
            displayChild.onChildsListClicked: {
                coverRectangle.windowLoader = listOfAllChildren
                coverRectangle.visible = true
            }
        }

        BotBar {
            id: botBarControl
            height: 0.15*parent.height
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.bottom: parent.bottom

            occurenceAddMain.onClicked: {
                addOccurenceForm.state = ""
                addOccurenceCover.visible = true
            }
        }

        Item {
            id: mainView
            anchors.top: topBarControl.bottom
            anchors.right: parent.right
            anchors.bottom: botBarControl.top
            anchors.left: parent.left

            Rectangle {
                id: rectangle1
                color: "#00000000"
                clip: true
                border.width: 2
                border.color: "#464646"
                anchors.rightMargin: 5
                anchors.leftMargin: 5
                anchors.bottomMargin: 5
                anchors.topMargin: 5
                anchors.fill: parent

                Show_Occurences {
                    id: shownOccurencesView
                    anchors.rightMargin: 2
                    anchors.leftMargin: 2
                    anchors.bottomMargin: 2
                    anchors.topMargin: 2
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    anchors.left: parent.left

                    aggregateList.model: Lista_Wydarzen.showOccurenceList
                }
            }
        }


        WindowCover {
            id: addOccurenceCover
            visible: false
            z:110
            OccurenceWorking {
                id: addOccurenceForm
                rootWidth: rootWindow.width
                rootHeight: rootWindow.height
                occurenciesList.model: Lista_Wydarzen.possibleOccurenceList
                workingOccurence: Lista_Wydarzen.workingOccurence
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                onClosed: parent.visible = false
            }
        }


        WindowCover {
            id: coverRectangle
            visible: false
            z:110
        }

        WindowCover {
            id: coverRect
            visible: false
            z:200
        }

        Component {
            id: childView

            ChildrenOccWindow {
                width:400
                height: root.height - 20 > 480 ? Math.min(root.height-80, 600) : root.height
                buttonClose.onClicked: {
                    if(!Lista_Wydarzen.isAnyChild()) state = "addFirst"
                    else coverRectangle.visible = false
                }
                workingChildOcc: Lista_Wydarzen.workingChild

                Component.onCompleted: {
                    if(!Lista_Wydarzen.isAnyChild()) state = "addFirst"
                    else state = ""
                }
            }
        }

        Component {
            id: listOfAllChildren

            ChildrenList {
                width:300
                maxHeight: root.height > 480 ? root.height-80 : root.height
                childrenList: Lista_Wydarzen.childrenList
                onChildChosen: {
                    Lista_Wydarzen.choseChild(childIndex)
                    coverRectangle.visible = false
                }
                buttonClose.onClicked: coverRectangle.visible = false
            }
        }

        Component {
            id: starttimePicker

            Time_picker {
                z: 11
                chosen_time: Lista_Wydarzen.startDayView
                inputHours.visible: false
                onAccepted: {
                    Lista_Wydarzen.setstartDayView(newTime)
                    coverRectangle.visible = false
                    var error_i = Lista_Wydarzen.readOccurenciesfromDB()
                    if (error_i === -1) {
                        messageDialog.text = qsTr("Something went wrong.")
                        messageDialog.open()
                        return
                    }
                    if (error_i === -2) {
                        messageDialog.text = qsTr("Wrong time to show chosen.")
                        messageDialog.open()
                        shownOccurencesView.aggregateList.model = Lista_Wydarzen.showOccurenceList
                        return
                    }
                }
            }
        }

        Component {
            id: endtimePicker

            Time_picker {
                z: 11
                chosen_time: Lista_Wydarzen.endDayView
                inputHours.visible: false
                onAccepted: {
                    Lista_Wydarzen.setendDayView(newTime)
                    coverRectangle.visible = false
                    var error_i = Lista_Wydarzen.readOccurenciesfromDB()
                    if (error_i === -1) {
                        messageDialog.text = qsTr("Something went wrong.")
                        messageDialog.open()
                        return
                    }
                    if (error_i === -2) {
                        messageDialog.text = qsTr("Wrong time to show chosen.")
                        messageDialog.open()
                        shownOccurencesView.aggregateList.model = Lista_Wydarzen.showOccurenceList
                        return
                    }
                }
            }
        }

        Component {
            id: newUser
            NewUser {
                z: 11
                onHide: {
                    coverRectangle.visible = false
                }
            }
        }
    }
}
