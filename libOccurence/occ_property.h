/*
 * VERSION = 0.10
 * This QObject contains features for occurence. It has some possible types and due to those types component works.
 * I think about 3 types: lista data and text input.
 * This class beyond properties shouldn't be used directly from QML.
 * */

#ifndef OCC_PROPERTY_H
#define OCC_PROPERTY_H

#include <QObject>
#include <QStringList>
#include <QDateTime>
#include <QVariant>

#include <QtAlgorithms>

class OccProperty : public QObject {
		Q_OBJECT
		Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
		Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
		Q_PROPERTY(QVariant defaultValue READ defaultValue WRITE setDefaultValue NOTIFY defaultValueChanged)
		Q_PROPERTY(QStringList ocOpOptions READ ocOpOptions NOTIFY ocOpOptionsChanged)

		QString m_name;
		QString m_type;
		QVariant m_default; // This is default container, but I use that as container of working value too
		QStringList m_ocOpOptions;


	public:
        explicit OccProperty(QObject *parent = 0);
        explicit OccProperty(QString name, QString type = "none", QObject *parent = 0);
        ~OccProperty();

		QString name() const {
			return m_name;
		}

		QString type() const {
			return m_type;
		}

		QVariant defaultValue() const {
			return m_default;
		}

		QStringList ocOpOptions() const {
			return m_ocOpOptions;
		}

		void append_Option(QString Option) {
			m_ocOpOptions.append(Option);
			emit ocOpOptionsChanged(ocOpOptions());
		}



        OccProperty *copyofProperty();



	signals:

		void nameChanged(QString arg);

		void typeChanged(QString arg);

		void defaultValueChanged(QVariant defaultValue);

		void ocOpOptionsChanged(QStringList arg);


	public slots:

		void setName(QString arg) {
			if (m_name != arg) {
				m_name = arg;
				emit nameChanged(arg);
			}
		}

		void setType(QString arg)	{
			if (m_type != arg) {
				m_type = arg;
				emit typeChanged(arg);
			}
		}

		void setDefaultValue(QVariant defaultValue) {
			if (m_default == defaultValue)
				return;
			m_default = defaultValue;
			emit defaultValueChanged(defaultValue);
		}
};

#endif // OCC_PROPERTY_H
