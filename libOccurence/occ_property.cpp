#include "occ_property.h"


OccProperty::OccProperty(QObject *parent) :
	QObject(parent)
	{
	m_default = QString();
}

OccProperty::OccProperty(QString name, QString type, QObject *parent) :
	QObject(parent), m_name(name), m_type(type)
	{
	m_default = QString();
}

OccProperty::~OccProperty() {
}

OccProperty *OccProperty::copyofProperty() {
	OccProperty* copyProperty = new OccProperty(m_name, m_type);
	if(!m_default.isNull()) copyProperty->setDefaultValue(m_default);
	for (int i = 0; i < m_ocOpOptions.size(); ++i) copyProperty->append_Option(m_ocOpOptions[i]);
	return copyProperty;
}
