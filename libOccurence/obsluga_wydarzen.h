/*
 * VERSION = 0.10
 * This QObject is used to expose occurence QObjects to QML.
 * It should manage occurences, load it from files and expose to QML.
 * From QML it should be possible to add occurences and so on.
 *
 * Methods:
 * import - loads occurences from xml file
 *
 * PATH_TO_XML is Macro defining XML file.
 * */

#ifndef Obsluga_Wydarzen_H
#define Obsluga_Wydarzen_H

#include <QObject>
#include <QQmlListProperty>
#include <QUrl>

#include <QtSql>

#include "libOccurence/occ_property.h"
#include "libOccurence/occurence.h"
#include "libOccurence/work_occ.h"
#include "libOccurence/show_occ.h"
#include "libOccurence/aggregate_show_occurence.h"
#include "libOccurence/sql_queries.h"
#include "libOccurence/child.h"

#include "rapidXML/rapidxml.hpp"
#include "rapidXML/rapidxml_utils.hpp"
#include "rapidXML/rapidxml_print.hpp"

// do wykasowania
#include <QDebug>


class Obsluga_Wydarzen : public QObject
{
		Q_OBJECT
		Q_PROPERTY(QQmlListProperty<occurence> possibleOccurenceList READ possibleOccurenceList CONSTANT)
		Q_PROPERTY(QQmlListProperty<aggregate_show_occurence> showOccurenceList READ showOccurenceList NOTIFY showOccurenceListChanged)
		Q_PROPERTY(occurence *workingOccurence READ workingOccurence WRITE setworkingOccurence NOTIFY workingOccurenceChanged)
		Q_PROPERTY(QDate startDayView READ startDayView WRITE setstartDayView NOTIFY startDayViewChanged)
        Q_PROPERTY(QDate endDayView READ endDayView WRITE setendDayView NOTIFY endDayViewChanged)
        Q_PROPERTY(child * workingChild READ workingChild NOTIFY workingChildChanged)
        Q_PROPERTY(QStringList childrenList READ childrenList WRITE setChildrenList NOTIFY childrenListChanged)


//		Menu services for adding occurencies
		QList<occurence*> m_possibleOccurenceList;
		QList<aggregate_show_occurence*> m_showOccurenceList;
		void setParentstoPossibleList(QObject *parent);
		void setParentstoShowList(QObject *parent);

		QDate m_startDayView;
		qint64 msec_startDayView() {
			QDateTime helpinkDT;
			helpinkDT.setDate(m_startDayView);
			return helpinkDT.toMSecsSinceEpoch();
		}
		QDate m_endDayView;
		qint64 msec_endDayView() {
			QDateTime helpinkDT;
			helpinkDT.setDate(m_endDayView);
			return helpinkDT.toMSecsSinceEpoch();
		}

		void startTime();

        bool startUser();

//		m_workingOccurence is occurence which features are displayed in menu.
//		It contains default features and those are chosen at beginning.
		work_occ *m_workingOccurence;

//		Data Base services for application
        bool startDatebase();
		QSqlDatabase db;
		QSqlQuery* query; // if multiple databases are set there it should go, but now it's one so it's used
		bool import_occurences();

		bool setPropertiesToWorkingOcc();
		occurence* searchforName(QString);

//      Children services
        child * m_workingChild;

        QStringList m_childrenList;
        QList <ChildStruct> ChildrenList;

        bool startChildren();

        void import_children();
        void startWorkingChild ();
        int working_user_id;
        bool new_default_child();
        int default_child_id;
        int working_child_id;

public:
		explicit Obsluga_Wydarzen(QObject *parent = 0);
		~ Obsluga_Wydarzen();


		static int countpossibleOccurenceList(QQmlListProperty<occurence> *lista);
		static occurence *atpossibleOccurenceList(QQmlListProperty<occurence> *lista, int i);
		QQmlListProperty<occurence> possibleOccurenceList();

		static int countshowOccurenceList(QQmlListProperty<aggregate_show_occurence> *lista);
		static aggregate_show_occurence *atshowOccurenceList(QQmlListProperty<aggregate_show_occurence> *lista, int i);
        QQmlListProperty<aggregate_show_occurence> showOccurenceList();


		QDate startDayView() const {
			return m_startDayView;
		}

		QDate endDayView() const {
			return m_endDayView;
		}


        child * workingChild() const {
            return m_workingChild;
        }

        QStringList childrenList() {
            m_childrenList.clear();
            for (int i=0; i<ChildrenList.size(); i++)
                m_childrenList << ChildrenList[i].childName;
            return m_childrenList;
        }



        occurence* workingOccurence() const {
            return (occurence*)m_workingOccurence;
        }

        Q_INVOKABLE void setWorkName(const QString tname, const QVariant newTime = QVariant());

//		Basic functions connecting SQL server
		Q_INVOKABLE int sentOccurenceToServer();
		Q_INVOKABLE int readOccurenciesfromDB();

//		Meedling with shown occurencies
		Q_INVOKABLE void loadShowtoWorkingOcc (int showOccID, QDate aggDay);
		Q_INVOKABLE int loadWorkingtoShowOcc();
		Q_INVOKABLE void deleteShowOcc();
		show_occ *searchForShowOcc(int showOccID, QDate aggDay);
		aggregate_show_occurence *searchforAggregate(QDate aggDay);

//		Debug functions
		bool readQueries();
		Q_INVOKABLE void wyswietl_shownOccurencies();


	public slots:
		void setworkingOccurence(occurence *workingOccurence) {
			if (m_workingOccurence == workingOccurence)	return;
			m_workingOccurence = (work_occ*)workingOccurence;
			emit workingOccurenceChanged(workingOccurence);
		}

		void setstartDayView(QDate startDayView) {
			if (m_startDayView == startDayView) return;
			m_startDayView = startDayView;
			emit startDayViewChanged(startDayView);
		}

		void setendDayView(QDate endDayView) {
			if (m_endDayView == endDayView) return;
			m_endDayView = endDayView;
			emit endDayViewChanged(endDayView);
		}

        void saveWorkingChild();
        void setNewChild();
        void deleteChild();
        void choseChild(int childIndex);
        void setVoidChild();
        bool isAnyChild();

        void setChildrenList(QStringList childrenList) {
            if (m_childrenList == childrenList)
                return;
            m_childrenList = childrenList;
            emit childrenListChanged(childrenList);
        }

	signals:
		void showOccurenceListChanged(QQmlListProperty<aggregate_show_occurence> showOList);
		void workingOccurenceChanged(occurence *workingOccurence);
		void startDayViewChanged(QDate startDayView);
        void endDayViewChanged(QDate endDayView);

        void childrenListChanged(QStringList childrenList);
        void workingChildChanged(child * workingChild);
        void noChild();
};

#endif // Obsluga_Wydarzen_H
