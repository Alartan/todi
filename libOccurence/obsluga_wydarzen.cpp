#include "libOccurence/obsluga_wydarzen.h"

using namespace rapidxml;

Obsluga_Wydarzen::Obsluga_Wydarzen(QObject *parent):
			QObject(parent)
	{
    m_workingOccurence = new work_occ(this);
    if (!startDatebase()) return;
    if (!startUser()) return;
    if (!startChildren()) return;
}

Obsluga_Wydarzen::~Obsluga_Wydarzen(){
    delete query;
    db.close();
}



/*Definitions for QMLListProperty purposes */

int Obsluga_Wydarzen::countpossibleOccurenceList(QQmlListProperty<occurence> *lista) {
    Obsluga_Wydarzen *obsluga = qobject_cast<Obsluga_Wydarzen *>(lista->object);
    if (obsluga) {
        return obsluga->m_possibleOccurenceList.count();
    }
    return false;
}
occurence *Obsluga_Wydarzen::atpossibleOccurenceList(QQmlListProperty<occurence> *lista, int i) {
    Obsluga_Wydarzen *obsluga = qobject_cast<Obsluga_Wydarzen *>(lista->object);
    if (obsluga) {
        return obsluga->m_possibleOccurenceList[i];
    }
    return NULL;
}
QQmlListProperty<occurence> Obsluga_Wydarzen::possibleOccurenceList() {
    return QQmlListProperty<occurence>(this, 0,\
                                       &Obsluga_Wydarzen::countpossibleOccurenceList,\
                                       &Obsluga_Wydarzen::atpossibleOccurenceList );
}
void Obsluga_Wydarzen::setParentstoPossibleList(QObject *parent) {
    for (int i = 0; i<m_possibleOccurenceList.count(); i++) {
        m_possibleOccurenceList[i]->setParent(parent);
    }
}



int Obsluga_Wydarzen::countshowOccurenceList(QQmlListProperty<aggregate_show_occurence> *lista) {
    Obsluga_Wydarzen *obsluga = qobject_cast<Obsluga_Wydarzen *>(lista->object);
    if (obsluga) {
        return obsluga->m_showOccurenceList.count();
    }
    return false;
}
aggregate_show_occurence *Obsluga_Wydarzen::atshowOccurenceList(QQmlListProperty<aggregate_show_occurence> *lista, int i){
    Obsluga_Wydarzen *obsluga = qobject_cast<Obsluga_Wydarzen *>(lista->object);
    if (obsluga) {
        return obsluga->m_showOccurenceList[i];
    }
    return NULL;
}
QQmlListProperty<aggregate_show_occurence> Obsluga_Wydarzen::showOccurenceList(){
    return QQmlListProperty<aggregate_show_occurence>(this, 0,\
                                                      &Obsluga_Wydarzen::countshowOccurenceList,\
                                                      &Obsluga_Wydarzen::atshowOccurenceList );
}
void Obsluga_Wydarzen::setParentstoShowList(QObject *parent) {
	for (int i = 0; i<m_showOccurenceList.count(); i++) {
		m_showOccurenceList[i]->setParent(parent);
	}
}



/* Function to start Service*/

bool Obsluga_Wydarzen::startDatebase() {
    QDir DBPath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/dane_dziecka_ex.database");
    qDebug() << DBPath.path();
    QFileInfo databaseFileInfo(DBPath.path());
    if (!databaseFileInfo.exists()) {
        if (!QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)).exists()) {
            if (!QDir().mkpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation))) {
                qDebug() << "Could not create path" << QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
                return false;
            }
        }
        if ( !QFile(QString(PATH_TO_DEF_DB)).copy(DBPath.path()) ) {
            qDebug() << "Could not copy default database";
            return false;
        }
        if ( !QFile::setPermissions(DBPath.path(),QFile::WriteOwner | QFile::ReadOwner)) {
            qDebug() << "Could not set proper permissions to DB file";
            return false;
        }
    }
	db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(DBPath.path());
    qDebug()<<"DB ready to open";
    if (!db.open()) {
        qDebug() << "DB could not be open" << db.databaseName();
        return false;
    }
    query = new QSqlQuery(db);
    query->setForwardOnly(true);
    if( !query->exec("PRAGMA foreign_keys = ON;")) qDebug() << query->executedQuery() << query->boundValues() << query->lastError();
    qDebug()<<DBPath.path() << "database ready to go";
    return true;
}

bool Obsluga_Wydarzen::startUser() {
    query -> prepare(READ_USER);
    if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
    if (query->next()) working_user_id = query->value(0).toInt();
    else {
        qDebug() << "No user in DB";
        return false;
    }
    import_occurences();
    return true;
}

bool Obsluga_Wydarzen::startChildren() {
    if (!new_default_child()) setVoidChild();
    else {
        working_child_id = default_child_id;
    }
    startWorkingChild();
    import_children();
    return true;
}

void Obsluga_Wydarzen::startTime() {
    query -> prepare(READ_DEFAULT_TIME);
    query->bindValue(":currentChild", working_child_id);
    if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
    if (!query->next()) m_endDayView = QDate::currentDate();
    else {
        qint64 addTime = query->value(0).toInt();
        m_endDayView = QDateTime::fromMSecsSinceEpoch(addTime*1000).date();
    }
    m_startDayView = m_endDayView.addDays(-7);
    emit endDayViewChanged(endDayView());
    emit startDayViewChanged(startDayView());
}


bool Obsluga_Wydarzen::import_occurences(){
/*	//odczyt pliku
	FILE *f = fopen(PATH_TO_XML, "rb");
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);

	char *raw_lista_xml = (char *)malloc(fsize + 1);
	fread(raw_lista_xml, fsize, 1, f);
	fclose(f);
	raw_lista_xml[fsize] = 0;
*/
	//import z bazy danych
    query->exec("SELECT user_configuration FROM users;");
	query->next();
	QString f = query->value(0).toString();
	long fsize = f.size();
	char *raw_lista_xml = (char *)malloc(fsize + 1);//query->value(0).toByteArray().data();
	strcpy(raw_lista_xml, f.toStdString().c_str()); // best method
	raw_lista_xml[fsize] = 0;

	xml_document<> lista_xml;
	lista_xml.parse<0>(raw_lista_xml);
	xml_node<> *workNode;
	xml_attribute<> *workAttribute;

	for (xml_node<> *occurenceNode = lista_xml.first_node(0);
		occurenceNode;
		occurenceNode = occurenceNode->next_sibling(0)) {
			workNode = occurenceNode->first_node("name");
				if (!workNode) continue;
			m_possibleOccurenceList.append(new occurence(QString(workNode->value()), this));
	//times added to every occurence
			m_possibleOccurenceList.last()->append_Property(new OccProperty("Start time", QString("date")));
			workAttribute = occurenceNode->first_attribute("type");
			m_possibleOccurenceList.last()->append_Property(new OccProperty("End time", QString("date")));

			for (xml_node<> *feature = occurenceNode->first_node("feature");
					feature;
					feature = feature->next_sibling("feature")) {
				workNode = feature->first_node("name");
				workAttribute = feature->first_attribute("type");
					if (!workNode || !workAttribute) continue;
				m_possibleOccurenceList.last()->append_Property(\
							new OccProperty(QString(workNode->value()), QString(workAttribute->value())));
				workNode = feature->first_node("default");
				if (workNode) m_possibleOccurenceList.last()->setLastPropertyDefault(QString(workNode->value()));

				for (xml_node<> *option = feature->first_node("option");
						option;
						option = option->next_sibling("option")) {
					workNode = option;
						if (!workNode) continue;
					m_possibleOccurenceList.last()->appendLastPropertyOption(QString(workNode->value()));
				}
			}
		}
	lista_xml.clear();
	delete[] raw_lista_xml;
	return true;
}



/*  CHILDREN FUNCTIONS
 *
 *
 *
 *
 *
 */

void Obsluga_Wydarzen::import_children() {
    query->prepare(READ_CHILDREN);
    query->bindValue(":currentUser", working_user_id);
    if( !query->exec() ) qDebug() << query->lastQuery() << query->lastError();
    ChildrenList.clear(); //enough, because it isn't list of pointers
    while (query->next()) {
        ChildrenList.append(ChildStruct(query->value(0).toInt(), query->value(1).toString()));
    }
    emit childrenListChanged(childrenList());
}

void Obsluga_Wydarzen::startWorkingChild () {
    QObject * delObject = new QObject(this);
    if (working_child_id != -1) {
        if (!m_workingChild) {
            m_workingChild->setParent(delObject);
        }
        query->prepare(READ_CHILD);
        query->bindValue(":currentChild", working_child_id);
        if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
        m_workingChild = new child(query, this);
    }
    QObject::connect(m_workingChild, SIGNAL(childEditConfirmed()), this, SLOT(saveWorkingChild()));
    QObject::connect(m_workingChild, SIGNAL(childNewConfirmed()), this, SLOT(setNewChild()));
    QObject::connect(m_workingChild, SIGNAL(childDeleteConfirmed()), this, SLOT(deleteChild()));
    emit workingChildChanged(workingChild());
    startTime();
    readOccurenciesfromDB();
    delObject->deleteLater();
}

bool Obsluga_Wydarzen::new_default_child() {
    query->prepare(NEW_DEFAULT_CHILD);
    query->bindValue(":currentUser", working_user_id);
    if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
    if (query->next()) {
        default_child_id = query->value(0).toInt();
        return true;
    }
    return false;
}

void Obsluga_Wydarzen::saveWorkingChild() {
    query->prepare(UPDATE_CHILD);
    QByteArray inByteArray;
    query->bindValue(":childName", m_workingChild->name());
    query->bindValue(":childSex", m_workingChild->childSex());
    query->bindValue(":childBirth", m_workingChild->birthDateTime().toMSecsSinceEpoch()/1000);
    if (m_workingChild->childsPhoto().isLocalFile()) {
        QFile file(m_workingChild->childsPhoto().toLocalFile());
        if (!file.open(QIODevice::ReadOnly)) return;
        inByteArray = file.readAll();
        file.close();
    }
    else {
        QBuffer inBuffer( &inByteArray );
        inBuffer.open( QIODevice::WriteOnly );
        m_workingChild->childsIcon().save( &inBuffer, "PNG" );
    }
    query->bindValue(":childPhoto", inByteArray);
    query->bindValue(":childInfo", m_workingChild->additionalInfo());
    query->bindValue(":currentChild", working_child_id);
    if( !query->exec() ) qDebug() << query->lastQuery() << query->lastError();
    import_children();
    startWorkingChild();
}

void Obsluga_Wydarzen::setNewChild() {
    query->prepare(WRITE_NEW_CHILD);
    QByteArray inByteArray;
    query->bindValue(":currentUser", working_user_id);
    query->bindValue(":childName", m_workingChild->name());
    query->bindValue(":childSex", m_workingChild->childSex());
    query->bindValue(":childBirth", m_workingChild->birthDateTime().toMSecsSinceEpoch()/1000);
    if (m_workingChild->childsPhoto().isLocalFile()) {
        QFile file(m_workingChild->childsPhoto().toLocalFile());
        if (!file.open(QIODevice::ReadOnly)) return;
        inByteArray = file.readAll();
        file.close();
    }
    else {
        QBuffer inBuffer( &inByteArray );
        inBuffer.open( QIODevice::WriteOnly );
        m_workingChild->childsIcon().save( &inBuffer, "PNG" );
    }
    query->bindValue(":childPhoto", inByteArray);
    query->bindValue(":childInfo", m_workingChild->additionalInfo());
    if( !query->exec() ) qDebug() << query->lastQuery() << query->lastError();
    import_children();
    query->prepare(NEW_WORKING_CHILD);
    query->bindValue(":currentUser", working_user_id);
    if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
    if (query->next()) working_child_id = query->value(0).toInt();
    else qDebug() << "Child didn't add to DB.";
    startWorkingChild();
}

void Obsluga_Wydarzen::deleteChild() {
    query->prepare(DELETE_CHILD_FEATURES);
    query->bindValue(":currentChild", working_child_id);
    if( !query->exec() ) qDebug() << query->lastQuery() << query->lastError();
    query->prepare(DELETE_CHILD_OCCURENCIES);
    query->bindValue(":currentChild", working_child_id);
    if( !query->exec() ) qDebug() << query->lastQuery() << query->lastError();
    query->prepare(DELETE_CHILD);
    query->bindValue(":currentChild", working_child_id);
    if( !query->exec() ) qDebug() << query->lastQuery() << query->lastError();
    startChildren();
}

void Obsluga_Wydarzen::choseChild(int childIndex) {
    working_child_id = ChildrenList[childIndex].childID;
    startWorkingChild();
}

void Obsluga_Wydarzen::setVoidChild() {
    working_child_id = -1;
    default_child_id = -1;
    m_workingChild = new child(working_user_id, this);
    emit noChild();
}

bool Obsluga_Wydarzen::isAnyChild() {
    if (working_child_id == -1) {
        emit noChild();
        return false;
    }
    return true;
}


/*  OCCURENCE FUNCTIONS
 *
 *
 *
 *
 *
 */

int Obsluga_Wydarzen::readOccurenciesfromDB() {
	if (m_endDayView < m_startDayView) return -2;
	QObject * delObject = new QObject(this);
	if (!m_showOccurenceList.isEmpty()) {
		setParentstoShowList(delObject);
		m_showOccurenceList.clear();
		emit showOccurenceListChanged(showOccurenceList());
	}
	QSqlQuery featureQuery(db);
	qint64 startofDay = msec_startDayView();
	qint64 endofDay = startofDay;
	qint64 endDayView = msec_endDayView(); // to not convert multiple times
	aggregate_show_occurence * aggregated_prepared_for_view;
	show_occ * occurenceToAppend;
	while(startofDay <= endDayView) {
		endofDay = (QDateTime::fromMSecsSinceEpoch(startofDay).addDays(1)).toMSecsSinceEpoch();
		aggregated_prepared_for_view = new aggregate_show_occurence(QDateTime::fromMSecsSinceEpoch(startofDay).date());
        query -> prepare(READ_OCC_QUERY);
        query->bindValue(":currentChild", working_child_id);
        query->bindValue(":startTime", startofDay/1000);
        query->bindValue(":endTime", endofDay/1000);
        if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
        while(query->next()) {
            featureQuery.prepare(READ_FEAT_QUERY);
            featureQuery.bindValue(":currentOccurence", query->value(0));
            if( !featureQuery.exec() ) qDebug() << featureQuery.executedQuery() << featureQuery.lastError();
			occurenceToAppend = new show_occ(query->value(2).toString(),\
														query->value(4).toInt(), query->value(5).toInt(), \
											 aggregated_prepared_for_view->agregateDay(), query->value(0).toInt(), \
											 &featureQuery);
			aggregated_prepared_for_view->append(occurenceToAppend);
		}
		m_showOccurenceList.append(aggregated_prepared_for_view);
		startofDay = endofDay;
	}
	emit showOccurenceListChanged(showOccurenceList());
	delObject->deleteLater();
	return true;
}




/* That function sets properties to set working occurence.
 * Later Working occurence is used to provide model for features and as a holder for set by user features.*/

void Obsluga_Wydarzen::setWorkName(const QString tname, const QVariant newTime){
	QObject * delQObject = new QObject(this);
	m_workingOccurence->setParent(delQObject);
	m_workingOccurence = new work_occ(tname, this);
	setPropertiesToWorkingOcc();
	qDebug() << m_workingOccurence->name() << "ustawione wydarzenie robocze";
    if (!newTime.isNull()) {
        QDateTime defaultTime = newTime.toDateTime();
        int minutes = defaultTime.time().minute();
        minutes = minutes - minutes%5;
        defaultTime.setTime(QTime(defaultTime.time().hour(), minutes));
        m_workingOccurence->setPropertyonDefault(0, defaultTime);
        m_workingOccurence->setPropertyonDefault(1, defaultTime);
    }
	emit workingOccurenceChanged(workingOccurence());
	delQObject->deleteLater();
}

bool Obsluga_Wydarzen::setPropertiesToWorkingOcc () {
	QString WorkingName = m_workingOccurence->name();
	if (WorkingName.isEmpty()) return false; // exception if setting are initialized before occurence
	occurence * OriginalOccurence = searchforName(WorkingName);
	if (OriginalOccurence == NULL) return false;
	QList<OccProperty*>::iterator WorkingIterator = OriginalOccurence->FirstIterator();
	do {
		m_workingOccurence->append_Property(OriginalOccurence->copyofProperty(&WorkingIterator));
	} while (OriginalOccurence->NextIterator(&WorkingIterator));
	return true;
}

occurence * Obsluga_Wydarzen::searchforName (QString WorkingName) {
	for (QList<occurence*>::iterator i = m_possibleOccurenceList.begin(); i != m_possibleOccurenceList.end(); ++i)
		if(((*i)->name()) == WorkingName)
			return *i;
	return NULL;
}

int Obsluga_Wydarzen::sentOccurenceToServer(){
	int error_i = m_workingOccurence -> prepareForDeploy(msec_startDayView(), msec_endDayView());
    if (error_i < 0) return error_i;
    query -> prepare(WRITE_WORK_OCC_QUERY);
    query->bindValue(":currentChild", working_child_id);
    query->bindValue(":occurenceName", m_workingOccurence->name());
    query->bindValue(":occurenceStart", m_workingOccurence->OccurenceTimeStart());
    query->bindValue(":occurenceEnd", m_workingOccurence->OccurenceTimeEnd());
    if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
	QList<OccProperty*>::iterator WorkingIterator = m_workingOccurence->FirstIterator();
    do {
        query -> prepare(WRITE_NEW_WORK_FEAT_QUERY);
        query->bindValue(":featureName", m_workingOccurence->OccPropertyName(&WorkingIterator));
        query->bindValue(":featureValue", m_workingOccurence->OccPropertyDefault(&WorkingIterator));
        if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
	} while (m_workingOccurence->NextIterator(&WorkingIterator));
	if (error_i == 2) return readOccurenciesfromDB();
	return error_i;
}

void Obsluga_Wydarzen::loadShowtoWorkingOcc(int showOccID, QDate aggDay) {
	QObject * delQObject = new QObject;
	m_workingOccurence->setParent(delQObject);
	//create working from name of occurence
	show_occ *locatedShownOcc;
	locatedShownOcc = searchForShowOcc(showOccID, aggDay);
	m_workingOccurence = new work_occ(locatedShownOcc->name(), this);
	m_workingOccurence->occId = showOccID;
    setPropertiesToWorkingOcc();
	//load defaulties from located occurence to working one
	QList<OccProperty*>::iterator WorkingIterator = m_workingOccurence->FirstIterator();
	do {
		locatedShownOcc->insertDefValue(WorkingIterator);
	} while (m_workingOccurence->NextIterator(&WorkingIterator));
    emit workingOccurenceChanged(workingOccurence());
    delQObject->deleteLater();
}

int Obsluga_Wydarzen::loadWorkingtoShowOcc() {
	int showOccID = m_workingOccurence->occId;
	int error_i = m_workingOccurence->prepareForDeploy(msec_startDayView(), msec_endDayView());
	if (error_i < 0) return error_i;
    qDebug() << m_workingOccurence->name() << "message sent to server";
    query -> prepare(UPDATE_WORK_OCC_QUERY);
    query->bindValue(":currentOccurence", showOccID);
    query->bindValue(":occurenceStart", m_workingOccurence->OccurenceTimeStart());
    query->bindValue(":occurenceEnd", m_workingOccurence->OccurenceTimeEnd());
    if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
    query -> prepare(DELETE_WORK_FEAT_QUERY);
    query->bindValue(":currentOccurence", showOccID);
    if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
	QList<OccProperty*>::iterator WorkingIterator = m_workingOccurence->FirstIterator(); // ten iterator do tablicy bóli
	do {
		qDebug() << "===>" << m_workingOccurence->OccPropertyName(&WorkingIterator) << ":";
        qDebug() << "=====>" << " Wartość " << m_workingOccurence->OccPropertyDefault(&WorkingIterator);
        query -> prepare(WRITE_UPDATED_WORK_FEAT_QUERY);
        query->bindValue(":currentOccurence", m_workingOccurence->occId);
        query->bindValue(":featureName", m_workingOccurence->OccPropertyName(&WorkingIterator));
        query->bindValue(":featureValue", m_workingOccurence->OccPropertyDefault(&WorkingIterator));
        if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
	} while (m_workingOccurence->NextIterator(&WorkingIterator));
	if (error_i == 2) return readOccurenciesfromDB();
	return error_i;
}

void Obsluga_Wydarzen::deleteShowOcc() {
    query -> prepare(DELETE_WORK_FEAT_QUERY);
    query->bindValue(":currentOccurence", m_workingOccurence->occId);
    if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
    query -> prepare(DELETE_WORK_OCC_QUERY);
    query->bindValue(":currentOccurence", m_workingOccurence->occId);
    if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
	readOccurenciesfromDB();
}

show_occ * Obsluga_Wydarzen::searchForShowOcc(int showOccID, QDate aggDay) {
	aggregate_show_occurence* workingAggregate = searchforAggregate(aggDay);
	if (workingAggregate) return workingAggregate->searchForShowOcc(showOccID);
	return NULL;
}

aggregate_show_occurence * Obsluga_Wydarzen::searchforAggregate(QDate aggDay) {
	for (int i = 0; i < m_showOccurenceList.count(); i++) {
		if (m_showOccurenceList.at(i)->agregateDay() == aggDay) {
			return m_showOccurenceList.at(i);
		}
	}
	return NULL;
}









/*
 *
 *
 * DEBUGGING FUNCTIONS
 *
 * */

bool Obsluga_Wydarzen::readQueries() {
    query -> prepare(WRITE_WORK_OCC_QUERY);
    query->bindValue(":currentChild", working_child_id);
    query->bindValue(":occurenceName", m_workingOccurence->name());
    query->bindValue(":occurenceStart", m_workingOccurence->OccurenceTimeStart());
    query->bindValue(":occurenceEnd", m_workingOccurence->OccurenceTimeEnd());
    if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
	QList<OccProperty*>::iterator WorkingIterator = m_workingOccurence->FirstIterator(); // ten iterator do tablicy bóli
    do {
        query -> prepare(WRITE_UPDATED_WORK_FEAT_QUERY);
        query->bindValue(":currentOccurence", m_workingOccurence->occId);
        query->bindValue(":featureName", m_workingOccurence->OccPropertyName(&WorkingIterator));
        query->bindValue(":featureValue", m_workingOccurence->OccPropertyDefault(&WorkingIterator));
        if( !query->exec() ) qDebug() << query->executedQuery() << query->lastError();
	} while (m_workingOccurence->NextIterator(&WorkingIterator));
	return true;
}

void Obsluga_Wydarzen::wyswietl_shownOccurencies() {
	readOccurenciesfromDB();
	QList<aggregate_show_occurence*>::iterator WorkingOccIterator = m_showOccurenceList.begin();
	do {
		qDebug() << (*WorkingOccIterator)->agregateDay() << "message received from server";
		QList<show_occ*>::iterator WorkingIterator = (*WorkingOccIterator)->FirstIterator();
		do {
			qDebug() << (*WorkingIterator)->name() << (*WorkingIterator)->inttimeStart() << (*WorkingIterator)->inttimeEnd();
		} while ((*WorkingOccIterator)->NextIterator(&WorkingIterator));
		WorkingOccIterator++;
	} while (WorkingOccIterator != m_showOccurenceList.end());
}
