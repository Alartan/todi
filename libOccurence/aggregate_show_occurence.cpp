#include "aggregate_show_occurence.h"

aggregate_show_occurence::aggregate_show_occurence(QObject *parent):
	QObject(parent), m_agregateDay()
{}

aggregate_show_occurence::aggregate_show_occurence(QDate agregateDay, QObject *parent):
	QObject(parent), m_agregateDay(agregateDay)
{}




void aggregate_show_occurence::append(show_occ* next_occ_to_show) {
	next_occ_to_show->setParent(this);
	m_list_shown_Occ.append(next_occ_to_show);
	emit list_shown_OccChanged(list_shown_Occ());
}




QQmlListProperty<show_occ> aggregate_show_occurence::list_shown_Occ() {
	return QQmlListProperty<show_occ>(this, 0, &aggregate_show_occurence::countlistShownOcc, &aggregate_show_occurence::atlistShownOcc);
}

int aggregate_show_occurence::countlistShownOcc(QQmlListProperty<show_occ> *lista){
	aggregate_show_occurence *helpingOcc = qobject_cast<aggregate_show_occurence*>(lista->object);
	if (helpingOcc)
		return helpingOcc->m_list_shown_Occ.count();
	return false;
}

show_occ *aggregate_show_occurence::atlistShownOcc(QQmlListProperty<show_occ> *lista, int i) {
	aggregate_show_occurence *helpingOcc = qobject_cast<aggregate_show_occurence*>(lista->object);
	if (helpingOcc)
		return helpingOcc->m_list_shown_Occ[i];
	return NULL;
}




show_occ *aggregate_show_occurence::searchForShowOcc(int showOccID) {
	for (int i = 0; i < m_list_shown_Occ.count(); i++) {
		if (m_list_shown_Occ[i]->occID() == showOccID) return m_list_shown_Occ[i];
	}
	return NULL;
}

