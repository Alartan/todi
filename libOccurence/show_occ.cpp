#include "show_occ.h"

show_occ::show_occ(QObject *parent) :
	occurence(parent)
	{	}

show_occ::show_occ(QString name, qint64 timeStart, qint64 timeEnd, QDate dayOcc, int id, QSqlQuery *features_query, QObject *parent):
	occurence(name, parent),
	m_inttimeStart(timeStart), m_inttimeEnd(timeEnd), m_occID(id), m_dayOcc(dayOcc),
	m_levelDisplay(-1)
	{
	while(features_query->next()) {
		m_listProper.append(new OccProperty(features_query->value(1).toString(), QString("text"), this));
		setLastPropertyDefault(features_query->value(2).toString());
	}
	emit listProperChanged(listProper());
}


show_occ::~show_occ() { }

void show_occ::insertDefValue(QList<OccProperty*>::iterator WorkingIterator) {
	if ((*WorkingIterator)->name() == "Start time") {
		(*WorkingIterator)->setDefaultValue(timeStart());
		return;
	}
	if ((*WorkingIterator)->name() == "End time") {
		(*WorkingIterator)->setDefaultValue(timeEnd());
		return;
	}
	for (QList<OccProperty*>::iterator i = m_listProper.begin(); i!=m_listProper.end(); i++) {
		if ((*WorkingIterator)->name() == (*i)->name()) {
			(*WorkingIterator)->setDefaultValue((*i)->defaultValue());
			return;
		}
	}
}
