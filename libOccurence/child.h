#ifndef CHILD_H
#define CHILD_H

#include <QObject>
#include <QDateTime>
#include <QtSql>
#include <QQuickImageProvider>

struct ChildStruct {
		int childID;
		QString childName;
		ChildStruct(int ID, QString Name) : childID(ID), childName(Name) {}
};

class child : public QObject
{
		Q_OBJECT
		Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
		Q_PROPERTY(QDateTime birthDateTime READ birthDateTime WRITE setBirthDateTime NOTIFY birthDateTimeChanged)
		Q_PROPERTY(bool childSex READ childSex WRITE setChildSex NOTIFY childSexChanged)
		Q_PROPERTY(QString additionalInfo READ additionalInfo WRITE setAdditionalInfo NOTIFY additionalInfoChanged)
		Q_PROPERTY(QUrl childsPhoto READ childsPhoto WRITE setChildsPhoto NOTIFY childsPhotoChanged)
		Q_PROPERTY(QDateTime addedDateTime READ addedDateTime NOTIFY addedDateTimeChanged)
        Q_PROPERTY(QString mainUser READ mainUser CONSTANT)

		QString m_name;
		QDateTime m_birthDateTime;
		bool m_childSex;
		QString m_additionalInfo;
		QUrl m_childsPhoto;
		QPixmap m_childsIcon;

		QDateTime m_addedDateTime;
		QString m_mainUser;

	public:
		explicit child(QObject *parent = 0);
		explicit child(int user, QObject *parent = 0);
		explicit child(QSqlQuery *query, QObject *parent = 0);
		~child();

		QPixmap childsIcon() { return m_childsIcon; }

		QString name() const {
			return m_name;
		}

		QDateTime birthDateTime() const {
			return m_birthDateTime;
		}

		bool childSex() const {
			return m_childSex;
		}

		QString additionalInfo() const {
			return m_additionalInfo;
		}

		QDateTime addedDateTime() const {
			return m_addedDateTime;
		}

		QString mainUser() const {
			return m_mainUser;
		}

		QUrl childsPhoto() const {
			return m_childsPhoto;
		}

	signals:

		void nameChanged(QString name);
		void birthDateTimeChanged(QDateTime birthDateTime);
		void childSexChanged(bool childSex);
		void additionalInfoChanged(QString additionalInfo);
		void addedDateTimeChanged(QDateTime addedDateTime);
		void childsPhotoChanged(QUrl childsPhoto);

		void childEditConfirmed();
		void childNewConfirmed();
		void childDeleteConfirmed();

	public slots:
		void setName(QString name)
		{
			if (m_name == name)
				return;

			m_name = name;
			emit nameChanged(name);
		}
		void setBirthDateTime(QDateTime birthDateTime) {
			if (m_birthDateTime == birthDateTime)
				return;
			m_birthDateTime = birthDateTime;
			emit birthDateTimeChanged(birthDateTime);
		}

		void setChildSex(bool childSex)
		{
			if (m_childSex == childSex)
				return;

			m_childSex = childSex;
			emit childSexChanged(childSex);
		}
		void setAdditionalInfo(QString additionalInfo)
		{
			if (m_additionalInfo == additionalInfo)
				return;

			m_additionalInfo = additionalInfo;
			emit additionalInfoChanged(additionalInfo);
		}
		void setChildsPhoto(QUrl childsPhoto)
		{
			if (m_childsPhoto == childsPhoto)
				return;

			m_childsPhoto = childsPhoto;
			emit childsPhotoChanged(childsPhoto);
		}
};

#endif // CHILD_H
