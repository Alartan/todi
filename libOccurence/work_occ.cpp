#include "libOccurence/work_occ.h"

work_occ::work_occ(QObject *parent) :
	occurence(parent)
	{	}

work_occ::work_occ(QString name, QObject *parent) :
    occurence(name, parent)
{	}

work_occ::~work_occ()
{   }

QString work_occ::OccurenceTimeStart() {
	return QString::number(inttimeStart);
}

QString work_occ::OccurenceTimeEnd() {
	return QString::number(inttimeEnd);
}

int work_occ::prepareForDeploy() {
	if (m_listProper.isEmpty()) return -1;
		QList<OccProperty*>::iterator Iterator = m_listProper.begin();
		QDateTime workTime = (*Iterator)->defaultValue().toDateTime();
		inttimeStart = workTime.toMSecsSinceEpoch()/1000;
		Iterator++;
		if ((*Iterator)-> type() != "void")
			workTime = (*Iterator)->defaultValue().toDateTime();
		inttimeEnd = workTime.toMSecsSinceEpoch()/1000;
	if (inttimeEnd<inttimeStart) return -2;
		m_listProper.removeFirst();
		m_listProper.removeFirst();
		emit listProperChanged(listProper());
	return 1;
}

int work_occ::prepareForDeploy(qint64 showStart, qint64 showEnd) {
	if (m_listProper.isEmpty()) return -1;
		QList<OccProperty*>::iterator Iterator = m_listProper.begin();
		QDateTime workTime = (*Iterator)->defaultValue().toDateTime();
		inttimeStart = workTime.toMSecsSinceEpoch()/1000;
		Iterator++;
		if ((*Iterator)-> type() != "void")
			workTime = (*Iterator)->defaultValue().toDateTime();
		inttimeEnd = workTime.toMSecsSinceEpoch()/1000;
	if (inttimeEnd<inttimeStart) return -2;
		m_listProper.removeFirst();
		m_listProper.removeFirst();
		emit listProperChanged(listProper());
	if (showStart<inttimeEnd || showEnd>inttimeStart) return 2;
    return 1;
}
