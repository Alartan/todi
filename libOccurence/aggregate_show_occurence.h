#ifndef AGGREGATE_SHOW_OCCURENCE_H
#define AGGREGATE_SHOW_OCCURENCE_H

#include <QObject>
#include <QQmlListProperty>
#include <QDate>
#include "libOccurence/show_occ.h"

class aggregate_show_occurence : public QObject
{
		Q_OBJECT
		Q_PROPERTY(QQmlListProperty<show_occ> list_shown_Occ READ list_shown_Occ NOTIFY list_shown_OccChanged)
		Q_PROPERTY(QDate agregateDay READ agregateDay WRITE setagregateDay NOTIFY agregateDayChanged)
		QList <show_occ*> m_list_shown_Occ;
		QDate m_agregateDay;

	public:
		explicit aggregate_show_occurence(QObject *parent = 0);
		explicit aggregate_show_occurence(QDate agregateDay, QObject *parent = 0);

		void append(show_occ *next_occ_to_show) ;

		QQmlListProperty<show_occ> list_shown_Occ();
		static int countlistShownOcc (QQmlListProperty<show_occ> *lista);
		static show_occ *atlistShownOcc (QQmlListProperty<show_occ> *lista, int i);

		QDate agregateDay() const {
			return m_agregateDay;
		}


		QList<show_occ*>::iterator FirstIterator() {
			return m_list_shown_Occ.begin();
		}

		bool NextIterator(QList<show_occ*>::iterator * WorkingIterator) {
			(*WorkingIterator)++;
			if (*WorkingIterator == m_list_shown_Occ.end()) return false;
			return true;
		}

		show_occ *searchForShowOcc(int showOccID);

	public slots:
		void setagregateDay(QDate agregateDay) {
			if (m_agregateDay == agregateDay)
				return;
			m_agregateDay = agregateDay;
			emit agregateDayChanged(agregateDay);
		}

	signals:
		void list_shown_OccChanged(QQmlListProperty<show_occ> list_shown_Occ);
		void agregateDayChanged(QDate agregateDay);
};

#endif // AGGREGATE_SHOW_OCCURENCE_H
