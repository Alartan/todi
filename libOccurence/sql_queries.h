#ifndef SQL_QUERIES_H
#define SQL_QUERIES_H


//#define PATH_TO_DB QDir::homePath()+"/.ToDi/dane_dziecka_ex.database"
//#define PATH_TO_DB "assets:/.ToDi/dane_dziecka_ex.database"
#define PATH_TO_DEF_DB ":/todi_def_DB"
#define PATH_TO_DEBUG_DB ":/debug/dane_dziecka_ex.database"


#define READ_DEFAULT_TIME "SELECT occurence_end_date FROM occurencies WHERE "\
        "child_id = :currentChild "\
        "ORDER BY occurence_end_date DESC LIMIT 1;"


#define READ_OCC_QUERY "SELECT * FROM occurencies " \
        "WHERE "\
        "(OCCURENCE_START_DATE AND OCCURENCE_START_DATE < :endTime ) "\
        "AND (:startTime < OCCURENCE_END_DATE AND OCCURENCE_END_DATE)"\
        "AND child_id = :currentChild;"

#define READ_FEAT_QUERY "SELECT * FROM features "\
        "WHERE occurence_id = :currentOccurence;"

#define WRITE_WORK_OCC_QUERY "INSERT INTO occurencies VALUES (NULL, "\
        ":currentChild, "\
        ":occurenceName, STRFTIME('%s','now'), " \
        ":occurenceStart, "\
        ":occurenceEnd);"

#define WRITE_NEW_WORK_FEAT_QUERY "INSERT INTO features VALUES ("  \
        "(SELECT occurence_id FROM occurencies ORDER BY occurence_id DESC LIMIT 1), "\
        ":featureName, "\
        ":featureValue);"

#define WRITE_UPDATED_WORK_FEAT_QUERY "INSERT INTO features VALUES ("  \
        ":currentOccurence, "\
        ":featureName, "\
        ":featureValue);"

#define DELETE_WORK_FEAT_QUERY "DELETE FROM features WHERE occurence_id = :currentOccurence; "

#define DELETE_WORK_OCC_QUERY "DELETE FROM occurencies WHERE occurence_id = :currentOccurence; "

#define UPDATE_WORK_OCC_QUERY "UPDATE occurencies " \
    "SET OCCURENCE_START_DATE = :occurenceStart, " \
    " OCCURENCE_END_DATE = :occurenceEnd " \
    "WHERE occurence_id = :currentOccurence; "




#define READ_CHILDREN "SELECT child_ID, child_name FROM children "\
	"WHERE user_id = :currentUser;"

#define READ_CHILD "SELECT * FROM children "\
	"WHERE child_id = :currentChild;"

#define UPDATE_CHILD "UPDATE children " \
	"SET child_name = :childName, "\
	"child_sex = :childSex, "\
	"birth_date = :childBirth, "\
	"photo = :childPhoto, "\
	"additional_description = :childInfo "\
	"WHERE child_id = :currentChild; "

#define WRITE_NEW_CHILD "INSERT INTO children  VALUES (NULL, "\
	":currentUser, "\
	":childName, STRFTIME('%s','now'), "\
	":childSex, "\
	":childBirth, "\
	":childPhoto, "\
	":childInfo);"

#define DELETE_CHILD "DELETE FROM children "\
	"WHERE child_id = :currentChild;"

#define DELETE_CHILD_FEATURES "DELETE FROM features "\
    "WHERE occurence_id IN (SELECT occurence_id FROM occurencies WHERE child_id = :currentChild);"

#define DELETE_CHILD_OCCURENCIES "DELETE FROM occurencies "\
    "WHERE child_id = :currentChild;"

#define NEW_DEFAULT_CHILD "SELECT child_id FROM children WHERE user_id = :currentUser ORDER BY child_id ASC LIMIT 1;"

#define NEW_WORKING_CHILD "SELECT child_id FROM children WHERE user_id = :currentUser ORDER BY creation_date DESC LIMIT 1;"




#define READ_MEASURMENTS "SELECT * FROM measurements WHERE child_id = :currentChild;"

#define UPDATE_MEASUREMENTS "UPDATE measurement " \
    "SET name = :measurementName," \
    "unit = :measuremntUnit "\
    "WHERE child_id = :currentChild" \
    "AND measuemrent_id = :measurmentId;"

#define WRITE_NEW_MEASUREMENT "INSERT INTO measurements VALUES (NULL, :currentChild, :measurementName, :measuremntUnit);"

#define DELETE_MEASUREMENT "DELETE FROM measurtements WHERE child_id = :currentChild AND measuemrent_id = :measurmentId;"



#define READ_USER "SELECT user_id FROM users;"

#endif // SQL_QUERIES_H
