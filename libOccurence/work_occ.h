#ifndef WORK_OCC_H
#define WORK_OCC_H

#include "libOccurence/occurence.h"

// do wykasowania
#include <QDebug>

class work_occ : public occurence
{
        Q_OBJECT

		qint64 inttimeStart;
		qint64 inttimeEnd;
        QDateTime m_defaultTime;

	public:
		explicit work_occ(QObject *parent = 0);
		explicit work_occ(QString name, QObject *parent = 0);
        ~work_occ();

        void setPropertyonDefault(int index, QVariant Option) {
			m_listProper[index]->setDefaultValue(Option);
			emit listProperChanged(listProper());
		}

		QString OccurenceTimeStart();
		QString OccurenceTimeEnd();


		int prepareForDeploy();
		int prepareForDeploy(qint64 showStart, qint64 showEnd);

        int occId;

    public slots:
    signals:
    private:
};

#endif // WORK_OCC_H
