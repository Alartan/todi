#include "libOccurence/occurence.h"

occurence::occurence(QObject *parent):
	QObject(parent), m_name()
{		}

occurence::occurence(QString name, QObject *parent):
	QObject(parent), m_name(name)
{		}



QQmlListProperty<OccProperty> occurence::listProper() {
	return QQmlListProperty<OccProperty>(this, 0, &occurence::countlistProper, &occurence::atlistProper);
}

int occurence::countlistProper(QQmlListProperty<OccProperty> *lista) {
	occurence *helpingOcc = qobject_cast<occurence*>(lista->object);
	if (helpingOcc)
		return helpingOcc->m_listProper.count();
	return false;
}

OccProperty *occurence::atlistProper(QQmlListProperty<OccProperty> *lista, int i){
	occurence *helpingOcc = qobject_cast<occurence*>(lista->object);
	if (helpingOcc)
		return helpingOcc->m_listProper[i];
	return NULL;
}
