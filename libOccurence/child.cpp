#include "child.h"

child::child(QObject *parent) : QObject(parent) {
}

//this is default child initialized only with user id number.
child::child(int user, QObject *parent) : QObject(parent) {
	m_name = "Name";
    m_birthDateTime = QDateTime::currentDateTime();
	m_childSex = 1;
	m_additionalInfo = "";
    m_childsPhoto = "qrc:/qml_custom/Images/Tux.png";
	m_addedDateTime = QDateTime();
	m_mainUser = user;
}

child::child(QSqlQuery *query, QObject *parent) : QObject(parent) {
	if(!query->next()) qDebug() << query->boundValue(":currentChild").toString();
	m_name = query->value(2).toString();
	qint64 workTime = query->value(5).toInt();
	m_birthDateTime.setMSecsSinceEpoch(workTime*1000);
	m_childSex = query->value(4).toInt();
	m_additionalInfo = query->value(7).toString();
	m_childsIcon.loadFromData(query->value(6).toByteArray());
	m_childsPhoto = "image://ToddlerImages/"+m_name+query->value(0).toString();

	workTime = query->value(3).toInt();
	m_addedDateTime.setMSecsSinceEpoch(workTime*1000);
	m_mainUser = query->value(1).toInt();
}

child::~child() {
}
