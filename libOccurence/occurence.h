/*
 * VERSION = 0.10
 * This is the basic class containing Occurences. It contains list of properties and it's QObject.
 * As it is it can be safely used to expose occurences to QML engine.
 * At now it has only few members allowing to load possible occurences from XML file.
 * */

#ifndef OCCURENCE_H
#define OCCURENCE_H

#include <QObject>
#include <QQmlListProperty>
#include "libOccurence/occ_property.h"

// do wykasowania
#include <QDebug>

class occurence : public QObject
{
		Q_OBJECT
		Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
		Q_PROPERTY(QQmlListProperty<OccProperty> listProper READ listProper NOTIFY listProperChanged)

	protected:
		QList<OccProperty*> m_listProper;
		QString m_name;

	public:
		explicit occurence(QObject *parent = 0);
		explicit occurence(QString name, QObject *parent = 0);

//		Obsługa Qproperty
		QQmlListProperty<OccProperty> listProper();
		static int countlistProper(QQmlListProperty<OccProperty> *lista);
		static OccProperty *atlistProper(QQmlListProperty<OccProperty> *lista, int i);

		QString name() const {
			return m_name;
		}

// it was in working occurence
		QString OccPropertyName(QList<OccProperty*>::iterator* WorkingIterator) {
			return (*(*WorkingIterator))->name();
		}

		QVariant OccPropertyDefault(QList<OccProperty*>::iterator * WorkingIterator) {
			return (*(*WorkingIterator))->defaultValue();
		}

//		Obsługa tworzenia wydarzenia dla parsera
		void append_Property(OccProperty * OcProperty) {
			OcProperty->setParent(this);
			m_listProper.append(OcProperty);
		}

		void appendLastPropertyOption(QString Option) {
			m_listProper.last()->append_Option(Option);
		}

		void setLastPropertyDefault(QString Option) {
			m_listProper.last()->setDefaultValue(Option);
		}

//		Obsługa zapisywania do wydarzenia roboczego
		OccProperty* copyofProperty(QList<OccProperty*>::iterator* WorkingIterator) {
			return (*(*WorkingIterator))->copyofProperty();
		}
		
		QList<OccProperty*>::iterator FirstIterator() {
			return m_listProper.begin();
		}

		bool NextIterator(QList<OccProperty*>::iterator * WorkingIterator) {
			(*WorkingIterator)++;
			if (*WorkingIterator == m_listProper.end()) return false;
			return true;
		}

		int NumberofProperties () {
			return m_listProper.size();
		}

	signals:

		void nameChanged(QString arg);

		void listProperChanged(QQmlListProperty<OccProperty> listProper);

	public slots:
		void setName(QString arg) {
			if (m_name != arg) {
				m_name = arg;
				emit nameChanged(arg);
			}
		}
};

#endif // OCCURENCE_H
