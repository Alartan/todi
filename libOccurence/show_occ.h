#ifndef SHOW_OCC_H
#define SHOW_OCC_H

#include "libOccurence/occurence.h"
#include <QDate>
#include <QtSql>


class show_occ : public occurence
{
		Q_OBJECT
		Q_PROPERTY(int inttimeStart READ inttimeStart WRITE setinttimeStart NOTIFY inttimeStartChanged)
		Q_PROPERTY(int inttimeEnd READ inttimeEnd WRITE setinttimeEnd NOTIFY inttimeEndChanged)
		Q_PROPERTY(int occID READ occID CONSTANT)
		Q_PROPERTY(int levelDisplay READ levelDisplay WRITE setlevelDisplay NOTIFY levelDisplayChanged)
		Q_PROPERTY(QDate dayOcc READ dayOcc CONSTANT)

		QDateTime timeStart() {
			return QDateTime::fromMSecsSinceEpoch(m_inttimeStart*1000);
		}
		QDateTime timeEnd() {
			return QDateTime::fromMSecsSinceEpoch(m_inttimeEnd*1000);
		}

	public:
        explicit show_occ(QObject *parent = 0);
		explicit show_occ(QString name, qint64 timeStart, qint64 timeEnd, QDate dayOcc, int id,\
						  QSqlQuery* features_query, QObject *parent = 0);

		~show_occ();

		int inttimeStart() const {
			QDateTime helpinkDT;
			helpinkDT.setMSecsSinceEpoch(m_inttimeStart*1000);
			return helpinkDT.toMSecsSinceEpoch()/1000 - m_dayOcc.toMSecsSinceEpoch()/1000;
		}

		int inttimeEnd() const
		{
			QDateTime helpinkDT;
			helpinkDT.setMSecsSinceEpoch(m_inttimeEnd*1000);
			return helpinkDT.toMSecsSinceEpoch()/1000 - m_dayOcc.toMSecsSinceEpoch()/1000;
		}

		int occID() const
		{
			return m_occID;
		}

		QDate dayOcc() const
		{
			return m_dayOcc.date();
		}

		void insertDefValue(QList<OccProperty*>::iterator WorkingIterator);

		int levelDisplay() const
		{
			return m_levelDisplay;
		}

	public slots:
		void setinttimeStart(int arg) {
			if (m_inttimeStart != arg) {
				m_inttimeStart = arg;
				emit inttimeStartChanged(arg);
			}
		}
		void setinttimeEnd(int arg) {
			if (m_inttimeEnd != arg) {
				m_inttimeEnd = arg;
				emit inttimeEndChanged(arg);
			}
		}

		void setlevelDisplay(int levelDisplay)
		{
			if (m_levelDisplay == levelDisplay)
				return;

			m_levelDisplay = levelDisplay;
			emit levelDisplayChanged(levelDisplay);
		}

	signals:
		void inttimeStartChanged(int inttimeStart);

		void inttimeEndChanged(int inttimeEnd);

		void levelDisplayChanged(int levelDisplay);

	private:
		qint64 m_inttimeStart;
		qint64 m_inttimeEnd;
		int m_occID;
		QDateTime m_dayOcc;
		int m_levelDisplay;
};

#endif // SHOW_OCC_H
