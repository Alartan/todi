<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>BotBarForm.ui</name>
    <message>
        <source>Add occurence</source>
        <translation type="unfinished">Dodaj wydarzenie</translation>
    </message>
</context>
<context>
    <name>ChildrenListForm.ui</name>
    <message>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
</context>
<context>
    <name>ChildrenMinatureForm.ui</name>
    <message>
        <source>Name can be very long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Button</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChildrenOccWindow</name>
    <message>
        <source>Chose photo for your child</source>
        <translation>Wybierz zdjęcie swojego dziecka</translation>
    </message>
</context>
<context>
    <name>ChildrenOccWindowForm.ui</name>
    <message>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Nowy</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>Zachowaj</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <source>Name: </source>
        <translation>Imię:</translation>
    </message>
    <message>
        <source>Born:  </source>
        <translation>Urodzony:</translation>
    </message>
    <message>
        <source>Parent user:</source>
        <translation>Użytkownik rodzic:</translation>
    </message>
    <message>
        <source>Added to application: </source>
        <translation>Dodany do aplikacji:</translation>
    </message>
    <message>
        <source>Additional informations: </source>
        <translation>Dodatkowe informacje:</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Imię:</translation>
    </message>
    <message>
        <source>Born: </source>
        <translation>Urodzony:</translation>
    </message>
    <message>
        <source>Additional informations:</source>
        <translation>Dodatkowe informacje:</translation>
    </message>
    <message>
        <source>Edit child: </source>
        <translation>Edycja dziecka</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Add at least one child</source>
        <translation>Dodaj pierwsze dziecko</translation>
    </message>
</context>
<context>
    <name>Show_Occurences</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Anuluj</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Usuń</translation>
    </message>
</context>
<context>
    <name>Time_pickerForm.ui</name>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Anuluj</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TopBarForm.ui</name>
    <message>
        <source>Show!</source>
        <translation type="unfinished">Pokaż!</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Anuluj</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Dane Dziecka</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Something went wrong.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wrong time to show chosen.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
