TEMPLATE = app

QT += qml quick widgets sql core

CONFIG += c++11

SOURCES += main.cpp \
    libOccurence/aggregate_show_occurence.cpp \
    libOccurence/obsluga_wydarzen.cpp \
    libOccurence/occurence.cpp \
    libOccurence/show_occ.cpp \
    libOccurence/work_occ.cpp \
    libOccurence/occ_property.cpp \
    libOccurence/child.cpp

RESOURCES += qml.qrc \
    default.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    libOccurence/aggregate_show_occurence.h \
    libOccurence/obsluga_wydarzen.h \
    libOccurence/occ_property.h \
    libOccurence/occurence.h \
    libOccurence/show_occ.h \
    libOccurence/work_occ.h \
    rapidXML/rapidxml.hpp \
    rapidXML/rapidxml_print.hpp \
    rapidXML/rapidxml_utils.hpp \
    libOccurence/child.h \
    libOccurence/sql_queries.h


lupdate_only {
SOURCES = *.qml \
        qml_custom/*.qml \
        qmlOccurence/*.qml\
        qmlOccurence/Children/*.qml\
        mainForm/*.qml
}

TRANSLATIONS = translations/todi_pl.ts \
            translations/todi_en.ts
