#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "libOccurence/obsluga_wydarzen.h"

class ChildImageProvider : public QQuickImageProvider
{
    Obsluga_Wydarzen *lista_Wydarzen;
public:
    ChildImageProvider(Obsluga_Wydarzen *listaWydarzen)
        : QQuickImageProvider(QQuickImageProvider::Pixmap) {
        lista_Wydarzen = listaWydarzen;
    }

    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize)
    {
        QPixmap result;
        QPixmap image = lista_Wydarzen->workingChild()->childsIcon();
        qDebug()<<id;

        if (requestedSize.isValid()) result = image.scaled(requestedSize, Qt::KeepAspectRatio);
        else result = image;
        *size = result.size();
        return result;
    }
};


int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

    qmlRegisterType<OccProperty>("alart.liboccurence",1,0,"OccProperty");
	qmlRegisterType<occurence>("alart.liboccurence",1,0,"Occurence");
	qmlRegisterType<Obsluga_Wydarzen>("alart.liboccurence",1,0,"Obsluga_Wydarzen");
    qmlRegisterType<work_occ>("alart.liboccurence",1,0,"Robocze_Wydarzenie");
    qmlRegisterType<show_occ>("alart.liboccurence",1,0,"ShowOcc");
    qmlRegisterType<aggregate_show_occurence>("alart.liboccurence",1,0,"AggregateShowOccurence");
    qmlRegisterType<child>("alart.liboccurence",1,0,"ChildOcc");

    QQmlApplicationEngine engine;

	Obsluga_Wydarzen Lista_Wydarzen;
	engine.rootContext()->setContextProperty("Lista_Wydarzen", &Lista_Wydarzen);

    engine.addImageProvider(QLatin1String("ToddlerImages"), new ChildImageProvider(&Lista_Wydarzen));
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

	return app.exec();
}
