import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import "qrc:/qmlOccurence/Children/"

TopBarForm {
    state: width<500 ? "short" : ""
    onWidthChanged: state= width<500 ? "short" : ""
    changeView.onClicked: state = "short1"
    hideChange.onClicked: state = "short"
}
