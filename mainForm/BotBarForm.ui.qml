import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Item {
    id: item1
    property alias occurenceAddMain: occurenceAddMain
    height: 120

    Button {
        id: occurenceAddMain
        y: 400
        height: 40
        text: qsTr("Add occurence")
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
    }
}
