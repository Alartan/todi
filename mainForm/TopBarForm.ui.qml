import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import "qrc:/qmlOccurence/Children/"

Item {
    id: item1
    property alias startTime: startTime
    property alias endTime: endTime
    property alias hideChange: hideChange
    property alias changeView: changeView
    property alias displayChild: displayChild
    height: 40
    z: 3

    GridLayout {
        id: gridLayout1
        anchors.topMargin: columnSpacing
        anchors.top: rectangle1.top
        anchors.leftMargin: rowSpacing
        anchors.left: rectangle1.left
        columnSpacing: 8
        rowSpacing: 20



        Button {
            id: hideChange
            text: qsTr("Cancel")
            visible: false
        }

        Button {
            id: changeView
            text: "Change shown range"
            visible: false
        }

        Button {
            id: startTime
            text: "Button"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }

        Button {
            id: endTime
            text: "Button"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }


    }

    ChildrenMinature {
        anchors.top: parent.top
        Layout.fillHeight: true
        Layout.minimumHeight: 22
        Layout.minimumWidth: 160
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

        id: displayChild
        anchors.bottom: parent.bottom
        anchors.right: parent.right
    }

    Rectangle {
        id: rectangle1
        color: "#ffffff"
        radius: parent.height/5
        anchors.top: parent.top
        anchors.left: parent.left
        z: -1
        border.color: "#400551"
        border.width: 2
        width: gridLayout1.childrenRect.width+gridLayout1.rowSpacing*2
        height: 40
    }
    states: [
        State {
            name: "short"

            PropertyChanges {
                target: changeView
                visible: true
            }

            PropertyChanges {
                target: startTime
                visible: false
            }

            PropertyChanges {
                target: endTime
                visible: false
            }

            PropertyChanges {
                target: rectangle1
                width: changeView.width+gridLayout1.rowSpacing*2
            }
        },
        State {
            name: "short1"

            PropertyChanges {
                target: rectangle1
                width: endTime.width+gridLayout1.rowSpacing*2
                height: gridLayout1.childrenRect.height+gridLayout1.columnSpacing*2
            }

            PropertyChanges {
                target: gridLayout1
                flow: GridLayout.TopToBottom
            }

            PropertyChanges {
                target: hideChange
                visible: true
            }
        }
    ]

}
